import sys
import os
import configparser
import numpy as np
from grasping_benchmarks.base.base_grasp_planner import BaseGraspPlanner, CameraData
from PIL import Image
# Import the LGPS implementation code
# Requires a LGPS_DIR environment variable pointing to the root of the repo
sys.path.append(os.environ['LGPS_DIR'])
os.chdir(os.environ['LGPS_DIR'])
from scripts.common.setup_stuff_util import setup_feature_extraction_stuff, setup_object_classifier, setup_classifier, setup_length_regressor
from scripts.common.drawings_util import generate_image_with_grasps
from scripts.grasps_generators.grasps_generators import get_length_in_meters

class LGPS:
    def __init__(self, cfg_file, source_dir = ''):

        sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), source_dir)))

        self.cfg = configparser.ConfigParser()
        self.cfg.read(cfg_file)

        # Additional configuration of the planner
        self.configure()

    def configure(self):
        self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')

        self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')

        self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.visualization = self.cfg.getboolean('visualization','visualization')
        self.representation_class = setup_feature_extraction_stuff(self.cfg)
        self.classifier_object = setup_object_classifier(self.cfg, self.representation_class)
        self.classifier = setup_classifier(self.cfg, self.representation_class)
        self.predict_gripper_opening = self.cfg.getboolean('length_regressor', 'activated')
        if self.predict_gripper_opening:
            self.length_regressor = setup_length_regressor(self.cfg, self.representation_class)
        else:
            self.length_regressor = None
        self.fix_value_meter_to_add = self.cfg.getfloat('length_regressor', 'fix_value_meter_to_add')

    def preprocess_depth(self, depth_raw, verbose=True):
        if verbose:
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))
        if self.change_too_far_to_cam:
            min = np.min(depth_raw)
            where_are_far = np.where(depth_raw > self.too_far_to_cam_threshold)
            depth_raw[where_are_far] = min
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_far] = value

        if self.change_nans:
            where_are_NaNs = np.isnan(depth_raw)
            if self.nans_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.nans_to_what == 'max':
                value = np.max(depth_raw)
            if self.nans_to_what == 'min':
                value = np.min(depth_raw)
            if self.nans_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_NaNs] = value

        if self.change_too_close_to_cam:
            where_are_close = np.where(depth_raw < self.too_close_to_cam_threshold)
            # where_are_close = np.where(depth_raw < 0.61)
            if self.too_close_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_close_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_close_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_close_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_close] = value

        if verbose:
            print('after')
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))

        return depth_raw

    def create_camera_data(self, rgb_image : np.ndarray, depth_image : np.ndarray, seg_mask : np.ndarray, cam_intrinsic_frame :str, cam_extrinsic_matrix : np.ndarray,
                           fx: float, fy: float, cx: float, cy: float, skew: float, w: int, h: int, obj_cloud : np.ndarray = None) -> CameraData:

        """Create the CameraData object in the format expected by the graspnet planner

        Parameters
        ----------
        rgb_image : np.ndarray
            RGB image
        depth_image : np.ndarray
            Depth (float) image
        cam_intrinsic_frame : str
            The reference frame ID of the images. Grasp poses are computed wrt this
            frame
        cam_extrinsic_matrix : np.ndarray
            The 4x4 camera pose in world reference frame
        fx : float
            Focal length (x direction)
        fy : float
            Focal length (y direction)
        cx : float
            Principal point (x direction)
        cy : float
            Principal poin (y direction)
        skew : float
            Skew coefficient
        w : int
            Image width
        h : int
            Image height
        obj_cloud : np.ndarray, optional
            Object point cloud to use for grasp planning (a segmented portion of
            the point cloud could be given here)

        Returns
        -------
        CameraData
            Object storing info required by plan_grasp()
        """
        '''
        #from source realsense
        def get_image_bundle(self):
        frames = self.pipeline.wait_for_frames()

        align = rs.align(rs.stream.color)
        aligned_frames = align.process(frames)
        color_frame = aligned_frames.first(rs.stream.color)
        aligned_depth_frame = aligned_frames.get_depth_frame()

        depth_image = np.asarray(aligned_depth_frame.get_data(), dtype=np.float32)
        depth_image *= self.scale
        color_image = np.asanyarray(color_frame.get_data())

        depth_image = np.expand_dims(depth_image, axis=2)

        return {
            'rgb': color_image,
            'aligned_depth': depth_image,
        }

        '''
        #todo check
        depth_image = np.expand_dims(depth_image, axis=2)

        camera_data = CameraData()
        camera_data.rgb_img = rgb_image

        if depth_image is not None:
            camera_data.depth_img = self.preprocess_depth(depth_image.copy())
        else:
            camera_data.depth_img = depth_image

        camera_data.seg_mask = seg_mask
        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])
        camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : w,
                                        'h' : h,
                                        'frame' : cam_intrinsic_frame,
                                        'matrix' : intrinsic_matrix
                                        }
        camera_data.extrinsic_matrix = cam_extrinsic_matrix
        camera_data.extrinsic_params['position'] = cam_extrinsic_matrix[:3,3]
        camera_data.extrinsic_params['rotation'] = cam_extrinsic_matrix[:3,:3]

        # Remove missing depth readouts
        # Filter out zeros and readings beyond 2m
        # np.nan_to_num(camera_data.depth_img, copy=False, nan=0)
        # invalid_mask = np.where(np.logical_or(camera_data.depth_img==0, camera_data.depth_img>2))
        # camera_data.depth_img[invalid_mask] = np.nan

        # Obtain the (colored) scene point cloud from valid points
        # self.scene_pc, selection = backproject(camera_data.depth_img,
        #                                   camera_data.intrinsic_params['matrix'],
        #                                   return_finite_depth = True,
        #                                   return_selection = True)
        # pc_colors = camera_data.rgb_img.copy()
        # pc_colors = np.reshape(pc_colors, [-1, 3])
        # pc_colors = pc_colors[selection, :]
        # self.scene_pc_colors = pc_colors
        #
        # # The algorithm requires an object point cloud, low-pass filtered over
        # # 10 frames. For now, we just use what comes from the camera.
        # # If a point cloud is passed to this function, use that as object pc for
        # # planning. Otherwise, use the scene cloud as object cloud
        # if obj_cloud is not None:
        #     self.object_pc = obj_cloud
        # else:
        #     self.object_pc = self.scene_pc

        # Not sure if we should return a CameraData object or simply assign it
        self._camera_data = camera_data
        return camera_data

    #work if all the grasps candidates are from the same scene
    def rerank(self, grasps_candidates_list, dict_rgb_scene = {}, dict_depth_raw = {}, n_candidates=None, sort=True, verbose=True):
        current_scene_id = grasps_candidates_list[0][1]
        import cv2
        cv2.imwrite('/home/yfleytou/catkin_ws_2/src/grasping-benchmarks-panda/camera_data_rgb_img.png', self._camera_data.rgb_img)

        if current_scene_id not in dict_rgb_scene:
            rgb_image = self._camera_data.rgb_img
            depth_image = self._camera_data.depth_img

            dict_rgb_scene[current_scene_id] = rgb_image
            dict_depth_raw[current_scene_id] = depth_image

        if self.classifier.by_object:
            # sort grasps_candidates by object prediction
            grasps_candidates_list, _, _, elapsed_time = self.classifier_object.predict(
                grasps_candidates_list,
                dict_rgb_scene, dict_depth_raw)

        scores, bvae_mu, bvae_var, elapsed_time = self.classifier.predict(grasps_candidates_list, dict_rgb_scene, dict_depth_raw)

        if verbose:
            print('scores: ', scores)
            print('bvae_mu: ', bvae_mu.shape)
            print('bvae_var: ', bvae_var.shape)
            print('elapsed_time: ', elapsed_time)

        if self.predict_gripper_opening and self.length_regressor.model_length is not None:
            if bvae_mu is None:
                predicted_lengths, bvae_mu, _, _ = self.length_regressor.predict_length(grasps_candidates_list, dict_rgb_scene, dict_depth_raw)
            else:
                predicted_lengths = self.length_regressor.predict_preprocessed_length(bvae_mu)

        #apply the new scores (and predicted_lengths)
        for i in range(len(scores)):
            '''
            id integer PRIMARY KEY,
            scene_id integer NOT NULL,
            user_id integer NOT NULL,
            x real NOT NULL,
            y real NOT NULL,
            depth real NOT NULL,
            euler_x real NOT NULL,
            euler_y real NOT NULL,
            euler_z real NOT NULL,
            length real NOT NULL,
            width real NOT NULL,
            quality real NOT NULL,
            FOREIGN KEY (scene_id) REFERENCES scenes (id),
            FOREIGN KEY (user_id) REFERENCES user (id)
            '''
            if self.predict_gripper_opening and self.length_regressor.model_length is not None:
                if not self.length_regressor.b_predict_correction:
                    grasps_candidates_list[i][9] = float(predicted_lengths[i])
                else:
                    if self.length_regressor.b_force_fix_prior:
                        grasps_candidates_list[i][9] = float(
                            predicted_lengths[i] + self.length_regressor.prior_length)
                    else:
                        grasps_candidates_list[i][9] = float(
                            predicted_lengths[i] + grasps_candidates_list[i][9])

            grasps_candidates_list[i][11] = scores[i]
            # print('self.grasps_candidates[scene_id]['+str(i)+']: ', self.grasps_candidates[scene_id][i])

        if sort:
            # print('predict_grasp_of_currently_loaded_grasps - get_candidates_sorted_by_scores')
            grasps_candidates_list = self.get_candidates_sorted_by_scores(grasps_candidates_list)

        #keep n_of_candidates grasps
        if n_candidates is not None:
            grasps_candidates_list = grasps_candidates_list[:n_candidates]

        self.grasps_candidates_list = grasps_candidates_list
        print('self.grasps_candidates_list ', len(self.grasps_candidates_list), ' : ', self.grasps_candidates_list)
        if self.visualization:
            self.visualize(self.grasps_candidates_list, dict_rgb_scene[current_scene_id])

        return grasps_candidates_list

    def visualize(self, grasps, rgb):
        """Visualize point cloud and last batch of computed grasps in a 2D visualizer
        """
        cv2_img = generate_image_with_grasps(grasps, rgb.copy(), destination=None, b_with_rectangles = False, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False)

        window_name = 'reranked grasps'
        img = Image.fromarray(cv2_img)
        img.show(window_name)
        input('enter any key to continue:')

    def get_list_of_lengths_in_meters(self, grasps_candidates_list):
        length_meters_list = []
        for grasp in grasps_candidates_list:
            # my method
            '''
            id integer PRIMARY KEY,
            scene_id integer NOT NULL,
            user_id integer NOT NULL,
            x real NOT NULL,
            y real NOT NULL,
            depth real NOT NULL,
            euler_x real NOT NULL,
            euler_y real NOT NULL,
            euler_z real NOT NULL,
            length real NOT NULL,
            width real NOT NULL,
            quality real NOT NULL,
            FOREIGN KEY (scene_id) REFERENCES scenes (id),
            FOREIGN KEY (user_id) REFERENCES user (id)
            '''
            x = grasp[3]
            y = grasp[4]
            z = grasp[5]
            angle = grasp[8]
            length_px = grasp[9]
            length_meters = self.get_length_in_meters( x, y, z, angle, length_px, self._camera_data.intrinsic_params)
            length_meters_list.append(length_meters)
        return length_meters_list


    def get_length_in_meters(self,  x, y, z, angle, length_dx, camera_intrinsics, verbose=True):
        return get_length_in_meters(x, y, z, angle, length_dx, camera_intrinsics, verbose=verbose)

    def get_candidates_sorted_by_scores(self, grasps_candidates, reverse=True):
        grasps_candidates.sort(key=lambda x: x[11], reverse=reverse)
        return grasps_candidates
