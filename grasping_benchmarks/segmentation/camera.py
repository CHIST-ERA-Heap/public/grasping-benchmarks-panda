import configparser
import cv2
import numpy as np
import sensor_msgs.point_cloud2
import math
import sys
import cv2
import rospy
import warnings
import message_filters
import tf2_ros
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from geometry_msgs.msg import Transform, Pose, PoseStamped, TransformStamped
from std_msgs.msg import Bool
import copy
from cv_bridge import CvBridge, CvBridgeError
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion

from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
from scipy.spatial.transform import Rotation as R
from grasping_benchmarks_ros.srv import Reranking, RerankingRequest

from grasping_benchmarks_ros.srv import Segmentation, SegmentationRequest, SegmentationResponse

import numpy as np
from cv_bridge import CvBridge
from geometry_msgs.msg import Point32, TransformStamped
from sensor_msgs.msg import CameraInfo, Image, PointCloud2, PointField
from std_msgs.msg import Header
import ast
import configparser
import cv2
import numpy as np
import sensor_msgs.point_cloud2
import math
import sys
import cv2
import rospy
import warnings
import message_filters
import tf2_ros
import sensor_msgs.point_cloud2
from sensor_msgs.point_cloud2 import read_points
from geometry_msgs.msg import Transform, Pose, PoseStamped, TransformStamped
from std_msgs.msg import Bool
import copy
from cv_bridge import CvBridge, CvBridgeError
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion

from grasping_benchmarks_ros.srv import GraspPlanner, GraspPlannerRequest, GraspPlannerResponse
from scipy.spatial.transform import Rotation as R
from grasping_benchmarks_ros.srv import Reranking, RerankingRequest
from grasping_benchmarks_ros.srv import Segmentation, SegmentationRequest, SegmentationResponse
import time
import numpy as np
from cv_bridge import CvBridge
from geometry_msgs.msg import Point32, TransformStamped
from sensor_msgs.msg import CameraInfo, Image, PointCloud2, PointField
from std_msgs.msg import Header

import tf


def get_grayscale_depth_from_raw(dst_d, min_depth=None, max_depth=None, b_Xue=False, missing_value=0, b_invert=False, max_depth_value = 0.7, blur=False):#https://arxiv.org/pdf/1604.05817.pdf

    invalid_px = np.where(dst_d > max_depth_value)
    dst_d[invalid_px] = max_depth_value

    if b_Xue:
        # dst_d = np.reshape(dst_d, (dst_d.shape[0], dst_d.shape[1], 1))
        im_data = cv2.copyMakeBorder(dst_d, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
        mask = (im_data == missing_value).astype(np.uint8)

        # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
        scale = np.abs(im_data).max()
        # print('scale: ', scale)
        im_data = im_data.astype(np.float32) / scale  # Has to be float32, 64 not supported.
        im_data = cv2.inpaint(im_data, mask, 1, cv2.INPAINT_NS)

        im_data = im_data.astype(np.float32)
        im_data = np.reshape(im_data, (im_data.shape[0], im_data.shape[1], 1))

        # Back to original size and value range.
        im_data = im_data[1:-1, 1:-1]
        im_data = im_data * scale
        im_data = np.asarray(im_data)
        im_data = im_data.reshape((im_data.shape[0], im_data.shape[1], 1))
        # print("min im_data: ", np.min(im_data))
        # print("mean im_data: ", np.mean(im_data))
        # print("max im_data: ", np.max(im_data))

    else:
        twobyte = False
        normalize = True
        BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        max_val = BINARY_IM_MAX_VAL
        if twobyte:
            max_val = np.iinfo(np.uint16).max

        if normalize:
            min_depth = np.min(dst_d)
            max_depth = np.max(dst_d)
            depth_data = (dst_d - min_depth) / (max_depth - min_depth)
            depth_data = float(max_val) * depth_data.squeeze()
        else:
            zero_px = np.where(dst_d == 0)
            zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
            depth_data = ((dst_d - min_depth) * \
                          (float(max_val) / (max_depth - min_depth))).squeeze()
            depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        im_data = np.zeros([dst_d.shape[0], dst_d.shape[1], 3])
        im_data[:, :, 0] = depth_data
        im_data[:, :, 1] = depth_data
        im_data[:, :, 2] = depth_data
        im_data = im_data.astype(np.uint8)


       # generate depth grayscale patch
        # cf autolab perception

        # BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        #
        # max_val = BINARY_IM_MAX_VAL
        #
        # min_depth = 0.0
        # # if min_depth is None:
        # #     min_depth = np.min(dst_d)
        #
        # if max_depth is None:
        #     max_depth = np.max(dst_d)
        #
        # depth_data = (dst_d - min_depth) / (max_depth - min_depth)
        # depth_data = float(max_val) * depth_data.squeeze()
        #
        # try:
        #     zero_px = np.where(dst_d == 0)
        #     zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
        # except(IndexError):
        #     pass
        #
        # depth_data = ((dst_d - min_depth) * \
        #               (float(max_val) / (max_depth - min_depth))).squeeze()
        # try:
        #     depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        # except(IndexError, TypeError):
        #     pass
        #
        # im_data = np.zeros([depth_data.shape[0], depth_data.shape[1], 3]).astype(np.uint8)
        # im_data[:, :, 0] = depth_data
        # im_data[:, :, 1] = depth_data
        # im_data[:, :, 2] = depth_data

        # if twobyte:
        #     return im_data.astype(np.uint16)
        # return im_data.astype(np.uint8)

        # depth_image = im_data.astype(np.uint8)
    if b_invert:
        im_data = 255.0 - im_data


    depth_image = im_data.astype(np.uint8)


    # print("depth_image.shape: ", depth_image.shape)
    if (depth_image.shape[2] == 3):
        depth_image = depth_image[:, :, 0]

    if blur:
        kernel = np.ones((5, 5), np.float32) / 25
        depth_image = cv2.filter2D(depth_image, -1, kernel)

    return depth_image

def pack_bgr(blue, green, red):
    # Pack the 3 BGR channels into a single UINT32 field as RGB.
    return np.bitwise_or(
        np.bitwise_or(
            np.left_shift(red.astype(np.uint32), 16),
            np.left_shift(green.astype(np.uint32), 8)), blue.astype(np.uint32))


def pack_bgra(blue, green, red, alpha):
    # Pack the 4 BGRA channels into a single UINT32 field.
    return np.bitwise_or(
        np.left_shift(blue.astype(np.uint32), 24),
        np.bitwise_or(
            np.left_shift(green.astype(np.uint32), 16),
            np.bitwise_or(
                np.left_shift(red.astype(np.uint32), 8), alpha.astype(
                    np.uint32))))

def convert_bgrd_to_pcl(bgr_image, depth_image, cam_info, rgb=False):
    cx = cam_info.K[2]
    cy = cam_info.K[5]
    fx = cam_info.K[0]
    fy = cam_info.K[4]

    center_x = cx
    center_y = cy

    constant_x = 1 / fx
    constant_y = 1 / fy
    if rgb:
        pointcloud_xyzrgb_fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
            PointField(name='rgb', offset=12, datatype=PointField.UINT32, count=1)
        ]
    else:
        pointcloud_xyz_fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)
        ]

    vs = np.array(
        [(v - center_x) * constant_x for v in range(0, depth_image.shape[1])])
    us = np.array(
        [(u - center_y) * constant_y for u in range(0, depth_image.shape[0])])

    # Convert depth from mm to m.
    depth_image = depth_image / 1000.0

    x = np.multiply(depth_image, vs)
    y = depth_image * us[:, np.newaxis]

    if rgb:
        stacked = np.ma.dstack((x, y, depth_image, bgr_image))

        compressed = stacked.compressed()
        pointcloud = compressed.reshape((int(compressed.shape[0] / 6), 6))

        pointcloud = np.hstack((pointcloud[:, 0:3],
                                pack_bgr(*pointcloud.T[3:6])[:, None]))
        pointcloud = [[point[0], point[1], point[2], int(point[3])]
                      for point in pointcloud]
    else:
        stacked = np.ma.dstack((x, y, depth_image))

        compressed = stacked.compressed()
        pointcloud = compressed.reshape((int(compressed.shape[0] / 3), 3))

        # pointcloud = np.hstack((pointcloud[:, 0:3],
        #                         pack_bgr(*pointcloud.T[3:6])[:, None]))
        pointcloud = [[point[0], point[1], point[2]]
                      for point in pointcloud]

    # print('pointcloud: ', pointcloud)
    header = Header()
    header.frame_id = "map"
    header.stamp = rospy.Time.now()

    if rgb:
        pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyzrgb_fields,
                                      pointcloud)
    pointcloud = sensor_msgs.point_cloud2.create_cloud(header, pointcloud_xyz_fields,
                                                       pointcloud)
    # pointcloud = sensor_msgs.point_cloud2.create_cloud(header=Header(), fields=pointcloud_xyzrgb_fields, points=pointcloud)

    return pointcloud


def read_intr_files(path_to_f):
    # importing the module

    # reading the data from the file
    with open(path_to_f) as f:
        data = f.read()

    # reconstructing the data as a dictionary
    d = ast.literal_eval(data)
    return d

class Camera:
    def __init__(self, camera_config_path):
        self.cfg = configparser.ConfigParser()
        self.cfg.read(camera_config_path)
        self.read_cfg()
        self.setup_camera()

    def read_cfg(self):
        self.fake_camera_info = self.cfg.get('camera','fake_camera_info')
        self.data_saving_folder = self.cfg.get('camera', 'data_saving_folder')
        self.camera_resolution_width = self.cfg.getint('camera', 'camera_resolution_width')
        self.camera_resolution_height = self.cfg.getint('camera', 'camera_resolution_height')
        self.camera_is_connected = self.cfg.getboolean('camera', 'camera_is_connected')
        self.rgb_extension = self.cfg.get('camera', 'rgb_extension')
        self.depth_extension = self.cfg.get('camera', 'depth_extension')
        #todo change to read with database
        # self.camera_intr_depth_filename = self.cfg.get('camera', 'camera_intr_depth_filename')
        # self.camera_intr_color_filename = self.cfg.get('camera', 'camera_intr_color_filename')

    def setup_camera(self):
        self.latest_rgb_image_path = self.data_saving_folder + self.rgb_extension
        self.latest_aligned_depth_path = self.data_saving_folder + self.depth_extension
        if self.camera_is_connected:
            # --- subscribers to camera topics --- #
            '''
            /camera/align_to_color/set_parameters
            /camera/aligned_depth_to_color/image_raw/compressed/set_parameters
            /camera/aligned_depth_to_color/image_raw/compressedDepth/set_parameters
            /camera/aligned_depth_to_color/image_raw/theora/set_parameters
            /camera/color/image_raw/compressed/set_parameters
            /camera/color/image_raw/compressedDepth/set_parameters
            /camera/color/image_raw/theora/set_parameters
            /camera/color/image_rect_color/compressed/set_parameters
            /camera/color/image_rect_color/compressedDepth/set_parameters
            /camera/color/image_rect_color/theora/set_parameters
            /camera/color_rectify_color/get_loggers
            /camera/color_rectify_color/set_logger_level
            /camera/color_rectify_color/set_parameters
            /camera/depth/image_rect_raw/compressed/set_parameters
            /camera/depth/image_rect_raw/compressedDepth/set_parameters
            /camera/depth/image_rect_raw/theora/set_parameters
            /camera/enable
            /camera/pointcloud/set_parameters
            /camera/points_xyzrgb_hw_registered/get_loggers
            /camera/points_xyzrgb_hw_registered/set_logger_level
            /camera/realsense2_camera/device_info
            /camera/realsense2_camera/get_loggers
            /camera/realsense2_camera/reset
            /camera/realsense2_camera/set_logger_level
            /camera/realsense2_camera_manager/get_loggers
            /camera/realsense2_camera_manager/list
            /camera/realsense2_camera_manager/load_nodelet
            /camera/realsense2_camera_manager/set_logger_level
            /camera/realsense2_camera_manager/unload_nodelet
            /camera/rgb_camera/auto_exposure_roi/set_parameters
            /camera/rgb_camera/set_parameters
            /camera/stereo_module/auto_exposure_roi/set_parameters
            /camera/stereo_module/set_parameters
            /rosout/get_loggers
            /rosout/set_logger_level
            '''
            self._cam_info_sub = message_filters.Subscriber('/camera/aligned_depth_to_color/camera_info', CameraInfo)
            self._rgb_sub = message_filters.Subscriber('/camera/color/image_raw', Image)
            self._depth_sub = message_filters.Subscriber('/camera/aligned_depth_to_color/image_raw', Image)
            self._pc_sub = message_filters.Subscriber('/camera/depth_registered/points', PointCloud2)
            #self._seg_sub = rospy.Subscriber('rgb/image_seg', Image, self.seg_img_callback, queue_size=10)

            # --- camera data synchronizer --- #
            self._tss = message_filters.ApproximateTimeSynchronizer([self._cam_info_sub, self._rgb_sub, self._depth_sub, self._pc_sub],
                                                                    queue_size=1, slop=0.5)
            self._tss.registerCallback(self._camera_data_callback)

            # --- robot/camera transform listener --- #
            self._tfBuffer = tf2_ros.buffer.Buffer()
            listener = tf2_ros.transform_listener.TransformListener(self._tfBuffer)

            # --- camera messages --- #
            self._cam_info_msg = None
            self._rgb_msg = None
            self._depth_msg = None
            self._pc_msg = None
            self._camera_pose = TransformStamped()
            self._root_reference_frame = 'panda_link0'
            self._new_camera_data = False

        else:
            self.fill_fake_camera_info()
            self._cam_info_msg_header_frame_id = "camera_depth_optical_frame"
            #todo investigate
            # Get the camera transform wrt the root reference frame of this class
            try:
                self._tfBuffer = tf2_ros.buffer.Buffer()
                self._root_reference_frame = 'panda_link0'
                # self._camera_pose = self._tfBuffer.lookup_transform(self._root_reference_frame, self._cam_info_msg.header.frame_id, rospy.Time())
                self._camera_pose = self._tfBuffer.lookup_transform(self._root_reference_frame, self._cam_info_msg_header_frame_id, rospy.Time())
            # except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            except Exception as e:
                warnings.warn("tf listener could not get camera pose. Are you publishing camera poses on tf?")
                self._camera_pose = TransformStamped()
                self._camera_pose.transform.rotation.w = -0.707
                self._camera_pose.transform.rotation.x = -0.707

            print('self._camera_pose: ', self._camera_pose)

        self.cv_bridge = CvBridge()

    def fill_fake_camera_info(self):
        self.cam_info = CameraInfo()
        if self.fake_camera_info == 'realsense':
            self.cam_info.width = 1280
            self.cam_info.height = 720

            self.cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
            self.cam_info.K = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 895.9815063476562,
                                              360.1416015625, 0.0, 0.0, 1.0]
            self.cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
            self.cam_info.P = [895.9815063476562, 0.0, 638.4413452148438, 0.0, 0.0, 895.9815063476562,
                                              360.1416015625, 0.0, 0.0, 0.0, 1.0, 0.0]
            self.cam_info.distortion_model = "plumb_bob"
            self.cam_info.binning_x = 0
            self.cam_info.binning_y = 0
            self.cam_info.roi.x_offset = 0
            self.cam_info.roi.y_offset = 0
            self.cam_info.roi.height = 0
            self.cam_info.roi.width = 0
            self.cam_info.roi.do_rectify = False

        elif self.fake_camera_info == 'primesense':
            self.cam_info.width = 640
            self.cam_info.height = 480
            self.cam_info.D = [0.0, 0.0, 0.0, 0.0, 0.0]
            self.cam_info.K = [525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0]
            self.cam_info.R = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
            self.cam_info.P = [525.0, 0.0, 319.5, 0.0, 0.0, 525.0, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0]
            self.cam_info.distortion_model = "plumb_bob"
            self.cam_info.binning_x = 0
            self.cam_info.binning_y = 0
            self.cam_info.roi.x_offset = 0
            self.cam_info.roi.y_offset = 0
            self.cam_info.roi.height = 0
            self.cam_info.roi.width = 0
            self.cam_info.roi.do_rectify = False

    # ------------------- #
    # Camera data handler #
    # ------------------- #
    def _camera_data_callback(self, cam_info, rgb, depth, pc):
        print('camera_data_callback')
        #todo check if you need some update
        self._cam_info_msg = cam_info
        self._rgb_msg = rgb
        self._depth_msg = depth
        self._pc_msg = pc


        # Get the camera transform wrt the root reference frame of this class
        try:
            self._camera_pose = self._tfBuffer.lookup_transform(self._root_reference_frame, self._cam_info_msg.header.frame_id, rospy.Time())
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            warnings.warn("tf listener could not get camera pose. Are you publishing camera poses on tf?")
            # self._camera_pose = TransformStamped()
            # self._camera_pose.transform.rotation.w = 1.0
            self._camera_pose = TransformStamped()
            self._camera_pose.transform.rotation.w = -0.707
            self._camera_pose.transform.rotation.x = -0.707

        self._new_camera_data = True

    def get_image(self, count_max=10000, verbose=True):
        # --- get images --- #
        if verbose:
            print("... waiting for images ...")

        count = 0
        while not self._new_camera_data and count < count_max:
            count += 1

        if count >= count_max:
            if verbose:
                print("...no images received")
            return Bool(False)
        else:
            if verbose:
                print("count: ", count)
                print('saving ', self.latest_rgb_image_path)
                print('saving ', self.latest_aligned_depth_path)

            try:
                cv2_color = self.cv_bridge.imgmsg_to_cv2(self._rgb_msg, desired_encoding='rgb8')

                raw_depth = self.cv_bridge.imgmsg_to_cv2(self._depth_msg, desired_encoding='passthrough')
                cv2_depth = np.array(raw_depth, dtype=np.float32)

                cv2_depth *= 0.001

                # Fix NANs
                nans_idx = np.isnan(cv2_depth)
                cv2_depth[nans_idx] = 1000.0

            except CvBridgeError as cv_bridge_exception:
                rospy.logerr(cv_bridge_exception)

            # Check for image and depth size
            if (cv2_color.shape[0] != cv2_depth.shape[0]) or (cv2_color.shape[1] != cv2_depth.shape[1]):
                msg = "Mismatch between depth shape {}x{} and color shape {}x{}".format(cv2_depth.shape[0],
                                                                                        cv2_depth.shape[1],
                                                                                        cv2_color.shape[0],
                                                                                        cv2_color.shape[1])
                rospy.logerr(msg)
                raise rospy.ServiceException(msg)

            self.latest_rgb_image = cv2_color
            self.latest_aligned_depth = cv2_depth

            cv2.imwrite(self.latest_rgb_image_path, self.latest_rgb_image)
            np.save(self.latest_aligned_depth_path, self.latest_aligned_depth)
            if verbose:
                depth_image = get_grayscale_depth_from_raw(self.latest_aligned_depth, max_depth_value=3.0)
                cv2.imwrite(self.data_saving_folder+'_gray.png', depth_image)

        self._new_camera_data = False

    def get_segmentation(self, cv_image, depth_raw, background_image=None, cam_info=None, verbose=False):

        #todo create pc_msg
        # PointCloud2
        pc_msg = convert_bgrd_to_pcl(cv_image, np.reshape(depth_raw*1000.0, (depth_raw.shape[0],depth_raw.shape[1])), cam_info)
        depth_msg = self.cv_bridge.cv2_to_imgmsg(depth_raw*1000.0, encoding="passthrough")
        rgb_msg = self.cv_bridge.cv2_to_imgmsg(cv_image, encoding="rgb8")#rgb8

        #load camera intrinsics extrinsics
        # create _rgb_msg with rgb_img
        # create _depth_msg with _depth_msg
        # create cam_info with cam_info_msg

        # Create the service request
        segmentation_req = SegmentationRequest()

        # Fill in the 2D camera data
        segmentation_req.color_image = rgb_msg
        if background_image is not None:
            background_msg = self.cv_bridge.cv2_to_imgmsg(background_image, encoding="rgb8")  # rgb8
            segmentation_req.background_image = background_msg
        segmentation_req.depth_image = depth_msg
        segmentation_req.camera_info = cam_info

        if verbose:
            print("... send request to server ...")

        # Plan for grasps
        try:
            reply = self._segmentation_planner(segmentation_req)
            if verbose:
                print("Service {} reply is: \n{}".format(self._segmentation_planner.resolved_name, reply))
        except rospy.ServiceException as e:
            print("Service {} call failed: {}".format(self._segmentation_planner.resolved_name, e))
            return Bool(False)

        reply = self.cv_bridge.imgmsg_to_cv2(reply.seg_image, desired_encoding='passthrough')
        cv2.imwrite('../../reply.png', reply)

if __name__ == '__main__':
    # Initialize the ROS node.
    rospy.init_node("Camera node launched")
    '''
    rgb_scene_path = '../data/test_scene/inria/security_glasses.png'
    depth_scene_path = '../data/test_scene/inria/security_glasses.npy'
    segmentation_path = '../data/test_scene/inria/security_glasses_mask.png'

    if rgb_scene_path is not None:
        cv_image = cv2.imread(rgb_scene_path)

    if depth_scene_path is not None:
        depth_raw = np.load(depth_scene_path)

    if segmentation_path is not None:
        segmentation = cv2.imread(segmentation_path, cv2.IMREAD_UNCHANGED)
        print('segmentation.shape: ', segmentation.shape)
        segmentation = np.reshape(segmentation, (cv_image.shape[0], cv_image.shape[1])).astype('uint8')
    else:
        segmentation = None
    '''
    config_path = 'test_camera.ini'
    cam = Camera(config_path)
    rospy.spin()

    cam.get_image()
    # Spin forever.



