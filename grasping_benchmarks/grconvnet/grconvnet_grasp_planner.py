from grasping_benchmarks.base.base_grasp_planner import BaseGraspPlanner, CameraData
from grasping_benchmarks.base.grasp import Grasp6D
from scipy.spatial.transform import Rotation as R
import torch
import sys
import os
import cv2
import configparser

# Import the GRConvNet implementation code
# Requires a GRCONVNET_DIR environment variable pointing to the root of the repo
sys.path.append(os.environ['GRCONVNET_DIR'])
os.chdir(os.environ['GRCONVNET_DIR'])
from utils.dataset_processing import image
from inference.models import get_network
from inference.post_process import post_process_output
from utils.dataset_processing.grasp import Grasp
import matplotlib.pyplot as plt
import numpy as np
from skimage.feature import peak_local_max
import math
from scipy.spatial.distance import cdist
from PIL import Image

class DepthImage(image.Image):
    def __init__(self, img):
        super().__init__(img)

    @classmethod
    def from_raw(cls, depth_raw):
        # depth_raw = np.load(fname)
        # depth_raw = depth_raw.reshape((depth_raw.shape[0], depth_raw.shape[1]))
        depth_raw = depth_raw.reshape((depth_raw.shape[0], depth_raw.shape[1]))
        return cls(depth_raw)

    def inpaint(self, missing_value=0):
        """
        Inpaint missing values in depth image.
        :param missing_value: Value to fill in teh depth image.
        """
        # cv2 inpainting doesn't handle the border properly
        # https://stackoverflow.com/questions/25974033/inpainting-depth-map-still-a-black-image-border
        self.img = cv2.copyMakeBorder(self.img, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
        mask = (self.img == missing_value).astype(np.uint8)

        # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
        scale = np.abs(self.img).max()
        self.img = self.img.astype(np.float32) / scale  # Has to be float32, 64 not supported.
        self.img = cv2.inpaint(self.img, mask, 1, cv2.INPAINT_NS)

        # Back to original size and value range.
        self.img = self.img[1:-1, 1:-1]
        self.img = self.img * scale

    def gradients(self):
        """
        Compute gradients of the depth image using Sobel filtesr.
        :return: Gradients in X direction, Gradients in Y diretion, Magnitude of XY gradients.
        """
        grad_x = cv2.Sobel(self.img, cv2.CV_64F, 1, 0, borderType=cv2.BORDER_DEFAULT)
        grad_y = cv2.Sobel(self.img, cv2.CV_64F, 0, 1, borderType=cv2.BORDER_DEFAULT)
        grad = np.sqrt(grad_x ** 2 + grad_y ** 2)

        return DepthImage(grad_x), DepthImage(grad_y), DepthImage(grad)

    def normalise(self):
        """
        Normalise by subtracting the mean and clippint [-1, 1]
        """
        self.img = np.clip((self.img - self.img.mean()), -1, 1)

def remove_too_close(input_centers, distance_pixel):
    output_centers = []
    print('input_centers: ', input_centers)
    avg_center = np.mean(input_centers, axis=0)
    avg_center = (int(avg_center[0]), int(avg_center[1]))
    print('avg_center: ', avg_center)
    output_centers.append(avg_center)
    for c in input_centers:
        np_c = np.asarray([c])
        dist = np.min(cdist(output_centers, np_c, metric='euclidean'))
        print('dist: ', dist)
        if dist > distance_pixel:
            output_centers.append(c)
    return output_centers

def detect_grasps(q_img, ang_img, width_img=None, no_grasps=1, min_distance=10, threshold_abs=0.02, ratio=1.0):
    """
    Detect grasps in a network output.
    :param q_img: Q image network output
    :param ang_img: Angle image network output
    :param width_img: (optional) Width image network output
    :param no_grasps: Max number of grasps to return
    :return: list of Grasps
    """
    local_max = peak_local_max(q_img, min_distance=min_distance, threshold_abs=threshold_abs, num_peaks=no_grasps)

    grasps = []
    grasp_qualities = []
    for grasp_point_array in local_max:
        grasp_point = tuple(grasp_point_array)

        grasp_angle = ang_img[grasp_point]
        grasp_quality = q_img[grasp_point]
        g = Grasp(grasp_point, grasp_angle)
        if width_img is not None:
            g.length = width_img[grasp_point]*ratio
            g.width = g.length / 2

        grasps.append(g)
        grasp_qualities.append(grasp_quality)

    return grasps, grasp_qualities

def get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                        dim_grasp_patch=96, dim_grasp_patch_y=None, verbose = False):
    rows, cols, canal = depth_data.shape
    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    if verbose:
        print("dim_grasp_patch: ", dim_grasp_patch)
        print("DIM: ", DIM)
        print("DIM_y: ", DIM_y)
        print("depth_data.shape: ", depth_data.shape)

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst_d = cv2.warpAffine(depth_data, M, (cols, rows))
    dst_d = dst_d[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
            int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    where_are_NaNs = np.isnan(dst_d)
    max_d = np.max(dst_d)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_are_NaNs] = max_d

    if verbose:
        print("dst_d.shape: ", dst_d.shape)
        print("mean dst_d: ", np.mean(dst_d))

    return dst_d  # raw depth

def get_depth_with_width(grasp_position_x, grasp_position_y, rotation_in_radian, cloud_points, gripper_length_pixel,
                         dim_grasp_patch_y=6, fix_delta=0.00, verbose=False):
    if verbose:
        print("grasp_position_x: ", grasp_position_x)
        print("grasp_position_y: ", grasp_position_y)
        print("rotation_in_radian: ", rotation_in_radian)
        print("gripper_length_pixel: ", gripper_length_pixel)
        print("cloud_points.shape: ", cloud_points.shape)
        print("max(cloud_points): ", np.max(cloud_points))

    if gripper_length_pixel <=5:
        gripper_length_pixel = 5
    points_on_line = get_raw_depth_patch(cloud_points, grasp_position_x, grasp_position_y,
                                        math.degrees(rotation_in_radian), dim_grasp_patch=gripper_length_pixel, dim_grasp_patch_y=dim_grasp_patch_y, verbose=verbose)

    if verbose:
        print("points_on_line: ", points_on_line)
        print("points_on_line.shape: ", points_on_line.shape)
        print("max(points_on_line): ", np.max(points_on_line))
    try:
        depth_min = np.min(points_on_line)
        depth_max = np.max(points_on_line)
    except Exception as e:
        print(e)
        depth_min = np.median(cloud_points)
        depth_max = depth_min

    return depth_min+fix_delta, depth_max+fix_delta


def find_center_of_mask(mask, reverse = True):
    centers = []

    print('mask.shape: ', mask.shape)
    print('mask min: ', np.min(mask))
    print('mask max ', np.max(mask))


    # Find contours:
    try:
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    except:
        # im, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for i in range(len(contours)):
        try:
            moments = cv2.moments(contours[i])
            centers.append((int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])))
            # centers.append([int(moments['m01'] / moments['m00']), (int(moments['m10'] / moments['m00']))])

        except:
            pass
    if len(centers) == 0:
        centers = [[int(mask.shape[0] / 2), int(mask.shape[1] / 2)]]

    # # Calculate image moments of the detected contour
    # M = cv2.moments(contours[0])
    # x = round(M['m10'] / M['m00'])
    # y = round(M['m01'] / M['m00'])
    # # center = (x, y)
    # center = (y, x)

    if reverse:
        for i in range(len(centers)):
            centers[i] = (centers[i][1], centers[i][0])

    return centers

def get_grasp_vector_with_rectangle(rectangle):
    # print("grasp :")
    # print(rectangle.shape)
    # print(rectangle)

    g_angle = get_angle(rectangle)
    g_width = get_width(rectangle)
    g_length = get_length(rectangle)
    g_center = get_center(rectangle)
    return float(g_center[1]), float(g_center[0]), float(g_angle), float(g_length), float(g_width)

def get_angle(points):
    """
    :return: Angle of the grasp to the horizontal.
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]

    angle = math.atan2(dy, dx)
    while (angle > np.pi):
        angle -= 2 * np.pi
    while (angle < -np.pi):
        angle += 2 * np.pi
    if angle > np.pi / 2:
        angle = -np.pi / 2 + (angle - np.pi / 2)

    if angle < -np.pi / 2:
        angle = np.pi / 2 + (angle - -np.pi / 2)

    return angle

def get_center(points):
    """
    :return: Rectangle center point
    """
    return points.mean(axis=0).astype(np.int)

def get_length(points):
    """
    :return: Rectangle length (i.e. along the axis of the grasp)
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

def get_width(points):
    """
    :return: Rectangle width (i.e. perpendicular to the axis of the grasp)
    """
    dy = points[2, 1] - points[1, 1]
    dx = points[2, 0] - points[1, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

class GRConvNetGraspPlanner(BaseGraspPlanner):
    """Grasp planner based on 6Dof-GraspNet

    """
    def __init__(self, cfg_file, grasp_offset=np.zeros(3), source_dir = ''):
        """Constructor

        Parameters
        ----------
        cfg_file : str, optional
            Path to config INI file, by default
            "cfg/config_graspnet.ini"
        grasp_offset : np.array, optional
            3-d array of x,y,z offset to apply to every grasp in eef
            frame, by default np.zeros(3)
        """
        sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), source_dir)))

        self.cfg = configparser.ConfigParser()
        self.cfg.read(cfg_file)

        # Call parent constructor
        super(GRConvNetGraspPlanner, self).__init__(self.cfg)

        # Additional configuration of the planner
        self._grasp_offset = grasp_offset
        self.configure()

    def configure(self):
        """Additional class configuration

        Parameters
        ----------
        cfg : dict configuration dict, as sourced from the YAML file
        """
        self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')

        self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')

        self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.visualization = self.cfg.getboolean('visualization','visualization')
        model_path = self.cfg.get('GRCONVNET', 'model_path')
        print('loading ', model_path)

        #params to extract candidates
        self.grconvnet_model_input_channels = self.cfg.getint('GRCONVNET', 'input_channels')
        if self.grconvnet_model_input_channels == 4:
            self.include_depth = True
            self.include_rgb = True
        elif self.grconvnet_model_input_channels == 3:
            self.include_depth = False
            self.include_rgb = True
        else:
            self.include_depth = True
            self.include_rgb = False

        self.min_distance_between_centers = self.cfg.getint('GRCONVNET', 'min_distance_between_centers')
        self.input_size = self.cfg.getint('GRCONVNET', 'input_size')
        self.output_size = self.cfg.getint('GRCONVNET', 'output_size')
        # self.width = self.cfg.getint('GRCONVNET', 'width')
        # self.height = self.cfg.getint('GRCONVNET', 'height')
        self.rot = self.cfg.getfloat('GRCONVNET', 'rot')
        self.zoom = self.cfg.getfloat('GRCONVNET', 'zoom')
        self.normalise = self.cfg.getboolean('GRCONVNET', 'normalise')
        self.grconvnet_dist = self.cfg.getint('GRCONVNET', 'dist')
        # self.grconvnet_no_grasps = self.cfg.getint('GRCONVNET', 'no_grasps')
        self.grconvnet_threshold = self.cfg.getfloat('GRCONVNET', 'threshold')


        print("grconvnet_grasps_interface model: ", model_path, ' exists: ', os.path.isfile(model_path))
        grconvnet_model_device_str = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.grconvnet_model_device = torch.device(grconvnet_model_device_str)
        grconvnet_model_name = self.cfg.get('GRCONVNET', 'model_name')

        network_fc = get_network(grconvnet_model_name)
        '''
        input_channels = Input image size for the network,
        dropout = Use dropout for training (1/0)')
        prob = Dropout prob for training (0-1)
        channel_size = channel_size Internal channel size for the network
        '''
        self.grconvnet_model = network_fc(
            input_channels=self.grconvnet_model_input_channels,
            dropout=0.0,
            prob=0.0,
            channel_size=1
        ).to(self.grconvnet_model_device)
        self.grconvnet_model = torch.load(model_path, map_location=self.grconvnet_model_device)

        self.grconvnet_model.eval()
        print("grconvnet_model loaded")

    def preprocess_depth(self, depth_raw, verbose=True):
        if verbose:
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))
        if self.change_too_far_to_cam:
            min = np.min(depth_raw)
            where_are_far = np.where(depth_raw > self.too_far_to_cam_threshold)
            depth_raw[where_are_far] = min
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_far] = value

        if self.change_nans:
            where_are_NaNs = np.isnan(depth_raw)
            if self.nans_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.nans_to_what == 'max':
                value = np.max(depth_raw)
            if self.nans_to_what == 'min':
                value = np.min(depth_raw)
            if self.nans_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_NaNs] = value

        if self.change_too_close_to_cam:
            where_are_close = np.where(depth_raw < self.too_close_to_cam_threshold)
            # where_are_close = np.where(depth_raw < 0.61)
            if self.too_close_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_close_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_close_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_close_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_close] = value

        if verbose:
            print('after')
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))

        return depth_raw

    def create_camera_data(self, rgb_image : np.ndarray, depth_image : np.ndarray, seg_mask : np.ndarray, cam_intrinsic_frame :str, cam_extrinsic_matrix : np.ndarray,
                           fx: float, fy: float, cx: float, cy: float, skew: float, w: int, h: int, obj_cloud : np.ndarray = None) -> CameraData:

        """Create the CameraData object in the format expected by the graspnet planner

        Parameters
        ----------
        rgb_image : np.ndarray
            RGB image
        depth_image : np.ndarray
            Depth (float) image
        cam_intrinsic_frame : str
            The reference frame ID of the images. Grasp poses are computed wrt this
            frame
        cam_extrinsic_matrix : np.ndarray
            The 4x4 camera pose in world reference frame
        fx : float
            Focal length (x direction)
        fy : float
            Focal length (y direction)
        cx : float
            Principal point (x direction)
        cy : float
            Principal poin (y direction)
        skew : float
            Skew coefficient
        w : int
            Image width
        h : int
            Image height
        obj_cloud : np.ndarray, optional
            Object point cloud to use for grasp planning (a segmented portion of
            the point cloud could be given here)

        Returns
        -------
        CameraData
            Object storing info required by plan_grasp()
        """
        '''
        #from source realsense
        def get_image_bundle(self):
        frames = self.pipeline.wait_for_frames()

        align = rs.align(rs.stream.color)
        aligned_frames = align.process(frames)
        color_frame = aligned_frames.first(rs.stream.color)
        aligned_depth_frame = aligned_frames.get_depth_frame()

        depth_image = np.asarray(aligned_depth_frame.get_data(), dtype=np.float32)
        depth_image *= self.scale
        color_image = np.asanyarray(color_frame.get_data())

        depth_image = np.expand_dims(depth_image, axis=2)

        return {
            'rgb': color_image,
            'aligned_depth': depth_image,
        }

        '''
        #todo check
        depth_image = np.expand_dims(depth_image, axis=2)

        camera_data = CameraData()
        camera_data.rgb_img = rgb_image
        if depth_image is not None:
            camera_data.depth_img = self.preprocess_depth(depth_image.copy())
        else:
            camera_data.depth_img = depth_image
        camera_data.seg_mask = seg_mask
        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])
        camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : w,
                                        'h' : h,
                                        'frame' : cam_intrinsic_frame,
                                        'matrix' : intrinsic_matrix
                                        }
        camera_data.extrinsic_params['position'] = cam_extrinsic_matrix[:3,3]
        camera_data.extrinsic_params['rotation'] = cam_extrinsic_matrix[:3,:3]

        # Remove missing depth readouts
        # Filter out zeros and readings beyond 2m
        # np.nan_to_num(camera_data.depth_img, copy=False, nan=0)
        # invalid_mask = np.where(np.logical_or(camera_data.depth_img==0, camera_data.depth_img>2))
        # camera_data.depth_img[invalid_mask] = np.nan

        # Obtain the (colored) scene point cloud from valid points
        # self.scene_pc, selection = backproject(camera_data.depth_img,
        #                                   camera_data.intrinsic_params['matrix'],
        #                                   return_finite_depth = True,
        #                                   return_selection = True)
        # pc_colors = camera_data.rgb_img.copy()
        # pc_colors = np.reshape(pc_colors, [-1, 3])
        # pc_colors = pc_colors[selection, :]
        # self.scene_pc_colors = pc_colors
        #
        # # The algorithm requires an object point cloud, low-pass filtered over
        # # 10 frames. For now, we just use what comes from the camera.
        # # If a point cloud is passed to this function, use that as object pc for
        # # planning. Otherwise, use the scene cloud as object cloud
        # if obj_cloud is not None:
        #     self.object_pc = obj_cloud
        # else:
        #     self.object_pc = self.scene_pc

        # Not sure if we should return a CameraData object or simply assign it
        self._camera_data = camera_data
        return camera_data

    @staticmethod
    def numpy_to_torch(s):
        if len(s.shape) == 2:
            return torch.from_numpy(np.expand_dims(s, 0).astype(np.float32))
        else:
            return torch.from_numpy(s.astype(np.float32))

    def get_depth(self, img, norm=True, center=None, rot=0.0, zoom=1.0, output_size=224):
        depth_img = DepthImage.from_raw(img)
        # depth_img = image.Image(img) #v1
        if center is not None and rot != 0.0:
            depth_img.rotate(rot, center)
        depth_img.crop(bottom_right=self.bottom_right, top_left=self.top_left)
        if zoom != 1.0:
            depth_img.zoom(zoom)
        depth_img.resize((output_size, output_size))
        if norm:
            depth_img.normalise()
            # depth_img.resize((self.output_size, self.output_size))
            # depth_img.img = depth_img.img.transpose((2, 0, 1)) #v1

        return depth_img.img

    def get_rgb(self, img, norm=True, center=None, rot=0.0, zoom=1.0, output_size=224):
        rgb_img = image.Image(img)
        if center is not None and rot != 0.0:
            rgb_img.rotate(rot, center)

        rgb_img.crop(bottom_right=self.bottom_right, top_left=self.top_left)
        if zoom != 1.0:
            rgb_img.zoom(zoom)
        rgb_img.resize((output_size, output_size))
        # rgb_img.resize((self.output_size, self.output_size))
        if norm:
            rgb_img.normalise()
            rgb_img.img = rgb_img.img.transpose((2, 0, 1))
        return rgb_img.img

    def get_data(self, rgb=None, depth=None, center=None, rot=0.0, zoom=1.0, output_size=224):
        depth_img = None
        rgb_img = None
        # Load the depth image
        if self.include_depth:
            depth_img = self.get_depth(img=depth, center=center, rot=rot, zoom=zoom, output_size=output_size)

        # Load the RGB image
        if self.include_rgb:
            rgb_img = self.get_rgb(img=rgb, center=center, rot=rot, zoom=zoom, output_size=output_size)

        if self.include_depth and self.include_rgb:
            '''
            x = self.numpy_to_torch(
                np.concatenate(
                    (np.expand_dims(depth_img, 0),
                     np.expand_dims(rgb_img, 0)),
                    1
                )
            )
            '''
            x = self.numpy_to_torch(
                np.concatenate(
                    (np.expand_dims(depth_img, 0),
                     rgb_img),
                    0
                )
            )
        elif self.include_depth:
            x = self.numpy_to_torch(depth_img)
        elif self.include_rgb:
            x = self.numpy_to_torch(np.expand_dims(rgb_img, 0))

        return x, depth_img, rgb_img

    def transform_grasp_to_6D(self, x, y, z, angle, camera_intrinsics):
        """Planar to 6D grasp pose
        Args:
            grasp_pose (obj:`gqcnn.grasping.Grasp2D` or :obj:`gqcnn.grasping.SuctionPoint2D`)
            camera_intrinsics (obj: `CameraIntrinsics`)
        Returns:
            cam_T_grasp (np.array(shape=(4,4))): 6D transform of the grasp pose wrt camera frame
        """
        '''
        camera_data.intrinsic_params = {
                                'fx' : fx,
                                'fy' : fy,
                                'cx' : cx,
                                'cy' : cy,
                                'skew' : skew,
                                'w' : w,
                                'h' : h,
                                'frame' : cam_intrinsic_frame,
                                'matrix' : intrinsic_matrix
                                }
        '''
        u = x - camera_intrinsics['cx']
        v = y - camera_intrinsics['cy']

        X = (z * u) / camera_intrinsics['fx']
        Y = (z * v) / camera_intrinsics['fy']

        grasp_pos = [X, Y, z]
        euler = [0, 0, -1.57 + angle]

        rot = R.from_euler('xyz', euler)
        cam_R_grasp = rot.as_dcm()

        cam_T_grasp = np.append(cam_R_grasp, np.array([grasp_pos]).T, axis=1)
        cam_T_grasp = np.append(cam_T_grasp, np.array([[0, 0, 0, 1]]), axis=0)

        grasp_target_T_panda_ef = np.eye(4)
        # grasp_target_T_panda_ef[2, 3] = -0.13
        grasp_target_T_panda_ef[:3, 3] = self._grasp_offset

        cam_T_grasp = np.matmul(cam_T_grasp, grasp_target_T_panda_ef)

        return cam_T_grasp


    def project_image_point_to_pointcloud(self, pixel_x, pixel_y, depth_meters, camera_intrinsics):
        u = pixel_x - camera_intrinsics['cx']
        v = pixel_y - camera_intrinsics['cy']

        X = (depth_meters * u) / camera_intrinsics['fx']
        Y = (depth_meters * v) / camera_intrinsics['fy']

        point_cloud_point = [X, Y, depth_meters]
        # print('point_cloud_point: ', point_cloud_point)

        return point_cloud_point

    def plan_grasp(self, camera_data, n_candidates=1):
        """Grasp planner.

        Parameters
        ---------
        req: :obj:`ROS ServiceRequest`
            ROS `ServiceRequest` for grasp planner service.
        """

        print('grconvnet_grasp_planner: plan_grasp')

        self._camera_data = camera_data
        grasp_list = []
        grasp_poses = []
        # self.grasp_poses = []
        # self.best_grasp = None

        rgb_image = camera_data.rgb_img.copy()
        depth_image = camera_data.depth_img.copy()
        shape = rgb_image.shape
        width = rgb_image.shape[1]
        height = rgb_image.shape[0]
        segmask = camera_data.seg_mask

        if segmask is None:
            print('missing segmask setting center as center of image')
            centres = [(int(height/2), int(width/2))]
        else:
            centres = find_center_of_mask(segmask)

        centres = remove_too_close(centres, distance_pixel=self.min_distance_between_centers)

        for center in centres:
            output_size = self.output_size
            input_size = self.input_size

            left = max(0, min(center[1] - input_size // 2, width - input_size))
            top = max(0, min(center[0] - input_size // 2, height - input_size))
            right = left + input_size
            bottom = top + input_size

            self.bottom_right = (bottom, right)
            self.top_left = (top, left)
            print('bottom_right: ', self.bottom_right)
            print('top_left: ', self.top_left)
            print('(input_size/output_size): ', (input_size/output_size))

            '''
            bottom_right:  (497, 986)
            top_left:  (49, 538)
            (input_size/output_size):  2.0
            '''

            x_camera_data, depth_img, rgb_img = self.get_data(rgb=rgb_image.copy(), depth=depth_image.copy(), center=center, rot=0.0, zoom=self.zoom, output_size=output_size)
            x_camera_data = x_camera_data.to(self.grconvnet_model_device)

            print('x_camera_data.size: ', list(x_camera_data.size()))

            with torch.no_grad():
                pred = self.grconvnet_model.predict(x_camera_data.unsqueeze(0))

            q_img, ang_img, width_img = post_process_output(pred['pos'], pred['cos'], pred['sin'],
                                                            pred['width'])

            gs = []
            current_threshold = self.grconvnet_threshold
            nb_of_try = -1
            while len(gs) < 1 and current_threshold > 0.009:
                nb_of_try+=1
                # if nb_of_try > 0:
                    # print('trying with current_threshold: ', current_threshold)

                '''
                def detect_grasps(q_img, ang_img, width_img=None, no_grasps=1):
                    """
                    Detect grasps in a network output.
                    :param q_img: Q image network output
                    :param ang_img: Angle image network output
                    :param width_img: (optional) Width image network output
                    :param no_grasps: Max number of grasps to return
                    :ret
                '''
                gs, grasp_qualities = detect_grasps(q_img, ang_img, width_img=width_img, no_grasps=n_candidates,
                                   min_distance=self.grconvnet_dist, threshold_abs=current_threshold, ratio=(input_size/output_size))
                current_threshold = current_threshold -0.01

            # gs = detect_grasps(q_img, ang_img, width_img=width_img, no_grasps=n_candidates)
            print("found ", len(gs), " grconvnet_grasps_interface grasps")

            if len(gs)!=0:
                # r = image.Image(rgb_image).img
                # d = image.Image(depth_image).img
                r = self.get_rgb(img=rgb_image, norm=False, center=center, rot=0.0, zoom=1.0, output_size=output_size)
                d = self.get_depth(img=depth_image, norm=False, center=center, rot=0.0, zoom=1.0, output_size=output_size)
                #self.visualise_gradients(d)
                d = np.reshape(d, (r.shape[0], r.shape[1]))

                #todo multiple figure opening but usefull for debbuging
                #if self.visualization:
                #    self.visualize(gs,r, d, q_img, ang_img, width_img)
                index_g = -1
                print('grasps: ')

                for g in gs:
                    index_g+=1
                    rectangle = g.as_gr.points
                    x_0, y_0, angle, length_px, width_px = get_grasp_vector_with_rectangle(rectangle)
                    print(x_0, y_0, angle, length_px, width_px)

                    # x = float(x_0 + self.top_left[1])
                    # y = float(y_0 + self.top_left[0])
                    x = float(x_0*(input_size/output_size) + self.top_left[1])
                    y = float(y_0*(input_size/output_size) + self.top_left[0])

                    quality = grasp_qualities[index_g]
                    if math.isnan(quality):
                        quality = 0.0

                    #get depth
                    depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                                grasp_position_y=y,
                                                                rotation_in_radian=angle,
                                                                cloud_points=camera_data.depth_img,
                                                                gripper_length_pixel=length_px,
                                                                verbose=True)

                    grconvnet_grasp = [x, y, depth_min, angle, length_px, width_px, float(quality)]

                    if grconvnet_grasp not in grasp_list:
                        grasp_list.append(grconvnet_grasp)

        #sort list
        reverse = True
        grasp_list.sort(key=lambda lol: lol[-1], reverse=reverse)
        '''
        grasp_list:  [[661.0, 439.0, 0.0, -0.12595014842986713, 29.210083633542606, 14.605041816771303, 0.8627533316612244]]

        '''
        print('grasp_list:')
        for g in grasp_list:
            print(g)
        if self.visualization:
            self.visualize_lines(grasp_list, rgb_image)

        for grconvnet_grasp in grasp_list:
            # my method
            # grconvnet_grasp = [x, y, depth_min, angle, length_px, width_px, float(quality)]
            x = grconvnet_grasp[0]
            y = grconvnet_grasp[1]
            z = grconvnet_grasp[2]
            angle = grconvnet_grasp[3]
            length_px = grconvnet_grasp[4]
            quality = grconvnet_grasp[6]
            length_meters = self.get_length_in_meters( x, y, z, angle, length_px, camera_data.intrinsic_params)
            # length_meters = 0.08
            pose_6d = self.transform_grasp_to_6D(x, y, z, angle, camera_data.intrinsic_params)
            pos = pose_6d[:3, 3]
            rot = pose_6d[:3, :3]

            '''
            position : (`numpy.ndarray` of float): 3-entry position vector wrt camera frame
            rotation (`numpy.ndarray` of float):3x3 rotation matrix wrt camera frame
            width : Distance between the fingers in meters.
            score: prediction score of the grasp pose
            ref_frame: frame of reference for camera that the grasp corresponds to.
            quaternion: rotation expressed as quaternion

            '''

            grasp_6D = Grasp6D(position=pos, rotation=rot,
                               width=length_meters, score=float(quality),
                               ref_frame=camera_data.intrinsic_params['frame'])
            grasp_poses.append(grasp_6D)

        # print('grasp_poses: ', grasp_poses)
        '''
        if len(grasp_poses)!=0:
            best_grasp = grasp_poses[0]
            return True
        else:
            return False
        '''
        return grasp_poses

    def get_length_in_meters(self,  x, y, z, angle, length_dx, camera_intrinsics, verbose=False):
        if verbose:
            print('x: ', x)
            print('y: ', y)
            print('z: ', z)
            print('angle: ', angle)
            print('length_dx: ', length_dx)


        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length_dx / 2)
        p2[0][0] = int(length_dx / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)

        p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
        p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))


        p_grasp_right = self.project_image_point_to_pointcloud(p2_recentered[0], p2_recentered[1], z, camera_intrinsics)

        p_grasp_left = self.project_image_point_to_pointcloud(p1_recentered[0], p1_recentered[1], z, camera_intrinsics)

        length_in_meters = np.linalg.norm(np.asarray(p_grasp_right) - np.asarray(p_grasp_left))

        if verbose:
            # print('center: ', p_grasp)
            print('right_finger: ', p_grasp_right)
            print('left_finger: ', p_grasp_left)
            print('length_in_meters: ', length_in_meters)

        return length_in_meters


    def visualise_gradients(self, depth_img):
        grad_x = cv2.Sobel(depth_img, cv2.CV_64F, 1, 0, borderType=cv2.BORDER_DEFAULT)
        grad_y = cv2.Sobel(depth_img, cv2.CV_64F, 0, 1, borderType=cv2.BORDER_DEFAULT)
        grad = np.sqrt(grad_x ** 2 + grad_y ** 2)
        rgb_gradients = np.dstack((np.asarray(grad_x), np.asarray(grad_y), np.asarray(grad))).astype(
            np.float32)

        # img is rgb, convert to opencv's default bgr
        img = cv2.cvtColor(rgb_gradients, cv2.COLOR_RGB2BGR)
        # img = rgb_gradients
        # Window name in which image is displayed
        window_name = 'grconvnet grasps'

        # Using cv2.imshow() method
        # Displaying the image
        cv2.imshow(window_name, img)

        # waits for user to press any key
        # (this is necessary to avoid Python kernel form crashing)
        cv2.waitKey(0)

        # closing all open windows
        cv2.destroyAllWindows()
        plt.close('all')



    def visualize(self, grasps, rgb, depth, q_img, ang_img, width_img):
        """Visualize point cloud and last batch of computed grasps in a 2D visualizer
        """

        plot_grasp( grasps,
                    rgb_img=rgb,
                    depth_img = depth,
                    grasp_q_img=q_img,
                    grasp_angle_img=ang_img,
                    grasp_width_img=width_img)

    def visualize_lines(self, grasps, rgb):
        """Visualize point cloud and last batch of computed grasps in a 2D visualizer
        """

        cv2_img = generate_image_with_grasps(grasps, rgb.copy(), destination=None, b_with_rectangles = False, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False)
        # cv2_img = cv2.cvtColor(cv2_img, cv2.COLOR_RGB2BGR)

        img =  Image.fromarray(cv2_img)
        window_name = 'grconvnet grasps'
        img.show(window_name)
        input('enter any key to continue:')


def plot_grasp(
        grasps,
        rgb_img=None,
        depth_img = None,
        grasp_q_img=None,
        grasp_angle_img=None,
        grasp_width_img=None
):
    """
    Plot the output grasp of a network
    :param fig: Figure to plot the output
    :param grasps: grasp pose(s)
    :param save: Bool for saving the plot
    :param rgb_img: RGB Image
    :param grasp_q_img: Q output of network
    :param grasp_angle_img: Angle output of network
    :param no_grasps: Maximum number of grasps to plot
    :param grasp_width_img: (optional) Width output of network
    :return:
    """
    fig = plt.figure(figsize=(10, 10))

    plt.ion()
    plt.clf()
    ax = fig.add_subplot(2, 3, 1)
    ax.imshow(rgb_img)
    ax.set_title('RGB')
    ax.axis('off')

    if depth_img is not None:
        ax = fig.add_subplot(2, 3, 2)
        ax.imshow(depth_img, cmap='gray')
        ax.set_title('Depth')
        ax.axis('off')

    ax = fig.add_subplot(2, 3, 3)
    ax.imshow(rgb_img)
    for g in grasps:
        g.plot(ax)
    ax.set_title('Grasp')
    ax.axis('off')

    ax = fig.add_subplot(2, 3, 4)
    plot = ax.imshow(grasp_q_img, cmap='jet', vmin=0, vmax=1)
    ax.set_title('Q')
    ax.axis('off')
    plt.colorbar(plot)

    ax = fig.add_subplot(2, 3, 5)
    plot = ax.imshow(grasp_angle_img, cmap='hsv', vmin=-np.pi / 2, vmax=np.pi / 2)
    ax.set_title('Angle')
    ax.axis('off')
    plt.colorbar(plot)

    ax = fig.add_subplot(2, 3, 6)
    plot = ax.imshow(grasp_width_img, cmap='jet', vmin=0, vmax=100)
    ax.set_title('Width')
    ax.axis('off')
    plt.colorbar(plot)

    # print("show")
    fig.canvas.draw()

    # convert canvas to image
    cv2_img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8,
            sep='')
    cv2_img  = cv2_img.reshape(fig.canvas.get_width_height()[::-1] + (3,))

    # img is rgb, convert to opencv's default bgr
    # # Window name in which image is displayed
    window_name = 'grconvnet grasps'
    # cv2_img = cv2.cvtColor(cv2_img, cv2.COLOR_RGB2BGR)

    img = Image.fromarray(cv2_img)
    img.show(window_name)
    input('enter any key to continue:')

#grconvnet_grasp = [x, y, depth_min, angle, length_px, width_px, float(quality)]
def generate_image_with_grasps(grasps, cv2_img, destination=None, b_with_rectangles = True, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False):
    scores = [g[6] for g in grasps]
    if normalize_scores:
        # norm = np.linalg.norm(scores)
        # scores = scores / norm
        current_maxval = np.max(scores)
        current_minval = np.min(scores)
    else:
        current_minval = 0.0
        current_maxval = 1.0
    # print("minval: ", current_minval)
    # print("minval: ", current_maxval)

    # for grasp in grasps:
    for i in range(len(grasps)):
        grasp = grasps[i]
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        x = grasp[0]
        y = grasp[1]
        angle = grasp[3]
        if default_color is None:
            # color = convert_to_rgb(grasp[11])
            color = convert_to_rgb(scores[i], minval=current_minval, maxval=current_maxval)

        else:
            color = default_color
        length = grasp[5]
        width = grasp[6]


        if b_with_rectangles:
            rectangle = grasp_to_rectangle(center=(y, x),
                                           angle=angle, length=length,
                                           width=width)

            rectangle = rectangle.astype('int32')

            blue = (255, 0, 0)
            yellow = (51, 255, 255)
            purple = (204, 204, 0)
            orange = (0, 128, 255)

            if b_with_circles:
                cv2_img = cv2.circle(cv2_img, (rectangle[0, 1], rectangle[0, 0]), 3, blue, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[1, 1], rectangle[1, 0]), 3, yellow, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[2, 1], rectangle[2, 0]), 3, orange, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[3, 1], rectangle[3, 0]), 3, purple, thickness=1, lineType=4)
            grasp_thickness = 1
            cv2_img = cv2.line(cv2_img, (rectangle[0, 1], rectangle[0, 0]), (rectangle[1, 1], rectangle[1, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[1, 1], rectangle[1, 0]), (rectangle[2, 1], rectangle[2, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[2, 1], rectangle[2, 0]), (rectangle[3, 1], rectangle[3, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[3, 1], rectangle[3, 0]), (rectangle[0, 1], rectangle[0, 0]),
                               color,
                               grasp_thickness, lineType=4)

        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length / 2)
        p2[0][0] = int(length / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)
        try:
            p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
            p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
        except:
            p1_recentered = (int(str(p1_rotated[0][0])) + int(str(x)), int(str(p1_rotated[1][0])) + int(str(y)))
            p2_recentered = (int(str(p2_rotated[0][0])) + int(str(x)), int(str(p2_rotated[1][0])) + int(str(y)))


        cv2_img = cv2.line(cv2_img, p1_recentered, p2_recentered,
                           color, thickness=thickness_line)#todo hardcoded

    if destination is not None:
        print("saving image to ", destination)
        cv2.imwrite(destination, cv2_img)

    return cv2_img

def convert_to_rgb(val, minval=0.0, maxval=1.0, colors=[(0, 0, 255), (255, 0, 0), (0, 255, 0)]):
    if val > maxval:
        val = maxval
    EPSILON = sys.float_info.epsilon  # Smallest possible difference.
    # "colors" is a series of RGB colors delineating a series of
    # adjacent linear color gradients between each pair.
    # Determine where the given value falls proportionality within
    # the range from minval->maxval and scale that fractional value
    # by the total number in the "colors" pallette.
    try:
        i_f = float(val - minval) / float(maxval - minval) * (len(colors) - 1)
    except Exception as e:
        print(e)
        i_f = 1.0
    # Determine the lower index of the pair of color indices this
    # value corresponds and its fractional distance between the lower
    # and the upper colors.
    try:
        i, f = int(i_f // 1), i_f % 1  # Split into whole & fractional parts.
    except Exception as e:
        print(e)
        f = 0.0
        i=0
    # Does it fall exactly on one of the color points?
    if f < EPSILON:
        return colors[i]
    else:  # Otherwise return a color within the range between them.
        (r1, g1, b1), (r2, g2, b2) = colors[i], colors[i + 1]
        return (int(r1 + f * (r2 - r1)), int(g1 + f * (g2 - g1)), int(b1 + f * (b2 - b1)))

def grasp_to_rectangle(center, angle, length, width, verbose = False):
    if verbose:
        print("from my_grasp_to_rectangle")
        print("center: ", center)
        print("angle: ", angle)
        print("length: ", length)
        print("width: ", width)

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))
    p3 = np.zeros((2, 1))
    p4 = np.zeros((2, 1))

    p1[0][0] = -int(length / 2)
    p1[1][0] = -int(width / 2)

    p2[0][0] = int(length / 2)
    p2[1][0] = -int(width / 2)

    p3[0][0] = int(length / 2)
    p3[1][0] = int(width / 2)

    p4[0][0] = -int(length / 2)
    p4[1][0] = int(width / 2)

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)
    p3_rotated = np.dot(rotation_matrix, p3)
    p4_rotated = np.dot(rotation_matrix, p4)

    p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
    p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
    p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
    p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

    # rectangle = np.array([p1_recentered,p2_recentered,p3_recentered,p4_recentered]).reshape((4,2)).astype(int)
    rectangle = np.array([p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
        (4, 2)).astype(int)
    return rectangle
