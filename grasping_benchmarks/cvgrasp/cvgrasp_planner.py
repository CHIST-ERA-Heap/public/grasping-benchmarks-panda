from grasping_benchmarks.base.base_grasp_planner import BaseGraspPlanner, CameraData
from grasping_benchmarks.base.grasp import Grasp6D
from scipy.spatial.transform import Rotation as R
import sys
import configparser
from skimage.morphology import skeletonize, medial_axis, remove_small_objects
from skimage.filters import threshold_otsu
from random import randint
from sklearn.cluster import MiniBatchKMeans
from skimage.segmentation import felzenszwalb
import math
import numpy as np
from numpy.linalg import norm
import os
import cv2
# class and function to work with a KdTree
from scipy.spatial.distance import cdist
from PIL import Image
sys.path.append(os.environ['LGPS_DIR'])
os.chdir(os.environ['LGPS_DIR'])
from scripts.cvgrasp.cvgrasp import find_candidates, rank_grasps, find_center_of_mask

class CVGraspPlanner(BaseGraspPlanner):
    """Grasp planner based on CV grasps

    """
    def __init__(self, cfg_file, grasp_offset=np.zeros(3), source_dir = ''):
        """Constructor

        Parameters
        ----------
        cfg_file : str, optional
            Path to config INI file, by default
            "cfg/config_graspnet.ini"
        grasp_offset : np.array, optional
            3-d array of x,y,z offset to apply to every grasp in eef
            frame, by default np.zeros(3)
        """
        sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), source_dir)))

        self.cfg = configparser.ConfigParser()
        self.cfg.read(cfg_file)

        # Call parent constructor
        super(CVGraspPlanner, self).__init__(self.cfg)

        # Additional configuration of the planner
        self._grasp_offset = grasp_offset
        self.configure()

        self.latest_grasps = []
        self.latest_grasp_scores = []


    def configure(self):
        """Additional class configuration

        Parameters
        ----------
        cfg : dict configuration dict, as sourced from the INI file
        """

        self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')

        self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')

        self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.type_of_generated_grasps = self.cfg.get('grasp_generation', 'type_of_generated_grasps')
        self.visualization = self.cfg.getboolean('visualization','visualization')

        self.get_gripper_width = self.cfg.getboolean('grasp_generation','get_gripper_width')
        self.min_gripper_width_pixel = self.cfg.getint('grasp_generation', 'min_gripper_width_pixel')
        self.max_gripper_width_pixel = self.cfg.getint('grasp_generation', 'max_gripper_width_pixel')
        self.fix_value_meter_to_add = self.cfg.getfloat('grasp_generation', 'fix_value_meter_to_add')

        self.keep_all = self.cfg.getboolean('grasp_generation','keep_all')

        #params to extract candidates
        self.prior_length = self.cfg.getfloat('grasp_generation', 'prior_length')
        self.prior_width = self.cfg.getfloat('grasp_generation', 'prior_width')
        #self.multiple_masks = self.cfg.getboolean('grasp_generation', 'multiple_masks')
        #self.count_grasp_total_min = self.cfg.getint('grasp_generation', 'count_grasp_total_min')
        #self.max_grasps_skel = self.cfg.getint('grasp_generation', 'max_grasps_skel')
        #self.max_grasps_contour = self.cfg.getint('grasp_generation', 'max_grasps_contour')
        #self.max_grasps_all = self.cfg.getint('grasp_generation', 'max_grasps_all')
        #self.max_distance_all_from_skel_edge = self.cfg.getint('grasp_generation', 'max_distance_all_from_skel_edge')
        #self.mu = self.cfg.getfloat('grasp_generation', 'mu')
        #self.sigma = self.cfg.getfloat('grasp_generation', 'sigma')
        #self.min_distance = self.cfg.getint('grasp_generation', 'min_distance')
        self.rank_grasps_by_distance = self.cfg.getboolean('grasp_generation', 'rank_grasps_by_distance')
        self.rank_grasps_by_distance_mode = self.cfg.get('grasp_generation', 'rank_grasps_by_distance_mode')
        self.rank_grasps_by_distance_dist_metric = self.cfg.get('grasp_generation', 'rank_grasps_by_distance_dist_metric')
        self.coef_score_dist_to_mask_center = self.cfg.getfloat('grasp_generation', 'coef_score_dist_to_mask_center')
        self.coef_score_dist_to_camera = self.cfg.getfloat('grasp_generation', 'coef_score_dist_to_camera')
        self.coef_score_smaller_width = self.cfg.getfloat('grasp_generation', 'coef_score_smaller_width')
        self.coef_distance_edge_centers = self.cfg.getfloat('grasp_generation', 'coef_distance_edge_centers')
        self.coef_score_target_width = self.cfg.getfloat('grasp_generation', 'coef_score_target_width')
        self.coef_score_dist_to_other_points = self.cfg.getfloat('grasp_generation', 'coef_score_dist_to_other_points')
        self.depth_color = self.cfg.get('grasp_generation', 'depth_color')
        self.default_score = self.cfg.getfloat('grasp_generation', 'default_score')

        #self.filter_candidates_too_wide = self.cfg.getboolean('grasp_generation', 'filter_candidates_too_wide')
        #self.filter_candidates_threshold_meter = self.cfg.getfloat('grasp_generation', 'filter_candidates_threshold_meter')
        self.keep_ratio = self.cfg.getint('grasp_generation', 'keep_ratio')
        self.nb_clusters_depth = self.cfg.getint('grasp_generation', 'nb_clusters_depth')
        self.nb_clusters_rgb = self.cfg.getint('grasp_generation', 'nb_clusters_rgb')
        self.augment_centers = self.cfg.getboolean('grasp_generation', 'augment_centers')
        self.remove_too_wide = self.cfg.getboolean('grasp_generation','remove_too_wide')
        self.skel_method = self.cfg.get('grasp_generation','skel_method')
        self.step_degrees = self.cfg.getint('grasp_generation', 'step_degrees')

        self.safe_slice = self.cfg.getboolean('grasp_generation','safe_slice')
        self.slice_height = self.cfg.getint('grasp_generation', 'slice_height')
        self.slice_width = self.cfg.getint('grasp_generation', 'slice_width')
        self.overlap_height_ratio = self.cfg.getfloat('grasp_generation', 'overlap_height_ratio')
        self.overlap_width_ratio = self.cfg.getfloat('grasp_generation', 'overlap_width_ratio')


        print("cv grasps parameters loaded")

    def preprocess_depth(self, depth_raw, verbose=False):
        if verbose:
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))
        if self.change_too_far_to_cam:
            min = np.min(depth_raw)
            where_are_far = np.where(depth_raw > self.too_far_to_cam_threshold)
            depth_raw[where_are_far] = min
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_far] = value

        if self.change_nans:
            where_are_NaNs = np.isnan(depth_raw)
            if self.nans_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.nans_to_what == 'max':
                value = np.max(depth_raw)
            if self.nans_to_what == 'min':
                value = np.min(depth_raw)
            if self.nans_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_NaNs] = value

        if self.change_too_close_to_cam:
            where_are_close = np.where(depth_raw < self.too_close_to_cam_threshold)
            # where_are_close = np.where(depth_raw < 0.61)
            if self.too_close_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_close_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_close_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_close_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_close] = value

        if verbose:
            print('after')
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))

        return depth_raw

    def create_camera_data(self, rgb_image : np.ndarray, depth_image : np.ndarray, seg_mask : np.ndarray, cam_intrinsic_frame :str, cam_extrinsic_matrix : np.ndarray,
                           fx: float, fy: float, cx: float, cy: float, skew: float, w: int, h: int, obj_cloud : np.ndarray = None) -> CameraData:

        """Create the CameraData object in the format expected by the graspnet planner

        Parameters
        ----------
        rgb_image : np.ndarray
            RGB image
        depth_image : np.ndarray
            Depth (float) image
        cam_intrinsic_frame : str
            The reference frame ID of the images. Grasp poses are computed wrt this
            frame
        cam_extrinsic_matrix : np.ndarray
            The 4x4 camera pose in world reference frame
        fx : float
            Focal length (x direction)
        fy : float
            Focal length (y direction)
        cx : float
            Principal point (x direction)
        cy : float
            Principal poin (y direction)
        skew : float
            Skew coefficient
        w : int
            Image width
        h : int
            Image height
        obj_cloud : np.ndarray, optional
            Object point cloud to use for grasp planning (a segmented portion of
            the point cloud could be given here)

        Returns
        -------
        CameraData
            Object storing info required by plan_grasp()
        """
        #todo check
        depth_image = np.expand_dims(depth_image, axis=2)

        camera_data = CameraData()
        camera_data.rgb_img = rgb_image
        if depth_image is not None:
            camera_data.depth_img = self.preprocess_depth(depth_image.copy())
        else:
            camera_data.depth_img = depth_image
        camera_data.seg_mask = seg_mask
        intrinsic_matrix = np.array([[fx, skew,  cx],
                                     [0,    fy,  cy],
                                     [0,     0,   1]])
        camera_data.intrinsic_params = {
                                        'fx' : fx,
                                        'fy' : fy,
                                        'cx' : cx,
                                        'cy' : cy,
                                        'skew' : skew,
                                        'w' : w,
                                        'h' : h,
                                        'frame' : cam_intrinsic_frame,
                                        'matrix' : intrinsic_matrix
                                        }
        camera_data.extrinsic_params['position'] = cam_extrinsic_matrix[:3,3]
        camera_data.extrinsic_params['rotation'] = cam_extrinsic_matrix[:3,:3]

        # Remove missing depth readouts
        # Filter out zeros and readings beyond 2m
        # np.nan_to_num(camera_data.depth_img, copy=False, nan=0)
        # invalid_mask = np.where(np.logical_or(camera_data.depth_img==0, camera_data.depth_img>2))
        # camera_data.depth_img[invalid_mask] = np.nan

        # Obtain the (colored) scene point cloud from valid points
        # self.scene_pc, selection = backproject(camera_data.depth_img,
        #                                   camera_data.intrinsic_params['matrix'],
        #                                   return_finite_depth = True,
        #                                   return_selection = True)
        # pc_colors = camera_data.rgb_img.copy()
        # pc_colors = np.reshape(pc_colors, [-1, 3])
        # pc_colors = pc_colors[selection, :]
        # self.scene_pc_colors = pc_colors
        #
        # # The algorithm requires an object point cloud, low-pass filtered over
        # # 10 frames. For now, we just use what comes from the camera.
        # # If a point cloud is passed to this function, use that as object pc for
        # # planning. Otherwise, use the scene cloud as object cloud
        # if obj_cloud is not None:
        #     self.object_pc = obj_cloud
        # else:
        #     self.object_pc = self.scene_pc

        # Not sure if we should return a CameraData object or simply assign it
        self._camera_data = camera_data
        return camera_data

    def transform_grasp_to_6D(self, x, y, z, angle, camera_intrinsics):
        """Planar to 6D grasp pose

        Args:
            grasp_pose (obj:`gqcnn.grasping.Grasp2D` or :obj:`gqcnn.grasping.SuctionPoint2D`)
            camera_intrinsics (obj: `CameraIntrinsics`)

        Returns:
            cam_T_grasp (np.array(shape=(4,4))): 6D transform of the grasp pose wrt camera frame
        """
        '''
        camera_data.intrinsic_params = {
                                'fx' : fx,
                                'fy' : fy,
                                'cx' : cx,
                                'cy' : cy,
                                'skew' : skew,
                                'w' : w,
                                'h' : h,
                                'frame' : cam_intrinsic_frame,
                                'matrix' : intrinsic_matrix
                                }
        '''
        u = x - camera_intrinsics['cx']
        v = y - camera_intrinsics['cy']

        X = (z * u) / camera_intrinsics['fx']
        Y = (z * v) / camera_intrinsics['fy']

        grasp_pos = [X, Y, z]
        euler = [0, 0, -1.57 + angle]

        rot = R.from_euler('xyz', euler)
        cam_R_grasp = rot.as_dcm()

        cam_T_grasp = np.append(cam_R_grasp, np.array([grasp_pos]).T, axis=1)
        cam_T_grasp = np.append(cam_T_grasp, np.array([[0, 0, 0, 1]]), axis=0)

        grasp_target_T_panda_ef = np.eye(4)
        # grasp_target_T_panda_ef[2, 3] = -0.13
        grasp_target_T_panda_ef[:3, 3] = self._grasp_offset

        cam_T_grasp = np.matmul(cam_T_grasp, grasp_target_T_panda_ef)

        return cam_T_grasp

    def project_image_point_to_pointcloud(self, pixel_x, pixel_y, depth_meters, camera_intrinsics):
        u = pixel_x - camera_intrinsics['cx']
        v = pixel_y - camera_intrinsics['cy']

        X = (depth_meters * u) / camera_intrinsics['fx']
        Y = (depth_meters * v) / camera_intrinsics['fy']

        point_cloud_point = [X, Y, depth_meters]
        #print('point_cloud_point: ', point_cloud_point)

        return point_cloud_point


    def plan_grasp(self, camera_data, n_candidates=1):
        """Grasp planner.

        Parameters
        ---------
        req: :obj:`ROS ServiceRequest`
            ROS `ServiceRequest` for grasp planner service.
        """

        print('cv_grasp_planner: plan_grasp')

        self._camera_data = camera_data

        rgb_image = camera_data.rgb_img
        rgb_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
        grasp_list = []
        grasp_poses = []
        depth_image = camera_data.depth_img
        segmask = camera_data.seg_mask

        if segmask is None:
            print('missing segmask messing')
            return False
        
        grasp_list, current_masks = find_candidates(rgb_image, segmask, depth_image, current_scene_id = -1, missing_types=self.type_of_generated_grasps, prior_length=self.prior_length, prior_width=self.prior_width,
                    get_gripper_width=self.get_gripper_width, min_gripper_width_pixel=self.min_gripper_width_pixel, max_gripper_width_pixel=self.max_gripper_width_pixel, keep_ratio=self.keep_ratio, nb_clusters_depth=self.nb_clusters_depth, nb_clusters_rgb=self.nb_clusters_rgb, augment_centers=self.augment_centers, skel_method=self.skel_method, remove_too_wide=self.remove_too_wide, step_degrees=self.step_degrees, safe_slice=self.safe_slice, slice_height = self.slice_height, slice_width = self.slice_width, overlap_height_ratio = self.overlap_height_ratio, overlap_width_ratio=self.overlap_width_ratio, depth_color=self.depth_color, default_score=self.default_score, keep_all=self.keep_all, verbose=False)

        '''
        grasp_list = find_candidates(rgb_image, segmask, depth_image,
                                          missing_types=self.type_of_generated_grasps, prior_length=self.prior_length, prior_width=self.prior_width,
                                          min_distance=self.min_distance,
                                          max_grasps_skel=self.max_grasps_skel, max_grasps_contour=self.max_grasps_contour, max_grasps_all=self.max_grasps_all,
                                          max_distance_all_from_skel_edge=self.max_distance_all_from_skel_edge, mu=self.mu, sigma=self.sigma,
                                          count_grasp_total_min=self.count_grasp_total_min,
                                          multiple_masks=self.multiple_masks, get_gripper_width=self.get_gripper_width, min_gripper_width_pixel=self.min_gripper_width_pixel,
                                          max_gripper_width_pixel=self.max_gripper_width_pixel
                                          )
        '''
        if self.rank_grasps_by_distance:

            grasp_list = rank_grasps(
                        depth_data=depth_image,
                        current_masks=current_masks, 
                        grasp_list=grasp_list,
                        coef_score_dist_to_mask_center=self.coef_score_dist_to_mask_center,
                        coef_score_dist_to_other_points=self.coef_score_dist_to_other_points,
                        coef_score_dist_to_camera=self.coef_score_dist_to_camera,
                        coef_score_smaller_width=self.coef_score_smaller_width,
                        coef_distance_edge_centers=self.coef_distance_edge_centers,
                        coef_score_target_width=self.coef_score_target_width,
                        target_width=self.prior_length,
                        too_far_to_cam_threshold=self.too_far_to_cam_threshold, rank_grasps_by_distance_mode=self.rank_grasps_by_distance_mode,
                        rank_grasps_by_distance_dist_metric=self.rank_grasps_by_distance_dist_metric, sort=True, reverse=True)


        #keep n_of_candidates grasps
        grasp_list = grasp_list[:n_candidates]
        #print('grasp_list:')
        #for g in grasp_list:
        #    print(g)
        # print('self.grasp_list: ', self.grasp_list)
        if self.visualization:
            self.visualize(grasp_list, rgb_image, segmask)

        # print('self.grasp_list: ', self.grasp_list)
        for cv_grasp in grasp_list:
            # my method
            # cv_grasp = [id_grasp, id_scene, user_id, x, y, float(depth_min), 0.0, 0.0, theta, length, width, label]
            x = cv_grasp[3]
            y = cv_grasp[4]
            z = cv_grasp[5]
            angle = cv_grasp[8]
            length_px = cv_grasp[9]
            length_meters = self.get_length_in_meters( x, y, z, angle, length_px, camera_data.intrinsic_params)
            quality = float(cv_grasp[11])

            pose_6d = self.transform_grasp_to_6D(x, y, z, angle, camera_data.intrinsic_params)
            pos = pose_6d[:3, 3]
            rot = pose_6d[:3, :3]

            '''
            position : (`numpy.ndarray` of float): 3-entry position vector wrt camera frame
            rotation (`numpy.ndarray` of float):3x3 rotation matrix wrt camera frame
            width : Distance between the fingers in meters.
            score: prediction score of the grasp pose
            ref_frame: frame of reference for camera that the grasp corresponds to.
            quaternion: rotation expressed as quaternion

            '''

            grasp_6D = Grasp6D(position=pos, rotation=rot,
                               width=length_meters, score=float(quality),
                               ref_frame=camera_data.intrinsic_params['frame'])
            grasp_poses.append(grasp_6D)

        # if len(self.grasp_poses)!=0:
        #     self.best_grasp = self.grasp_poses[0]
        #     return True
        # else:
        #     return False
        return grasp_poses

    def get_length_in_meters(self,  x, y, z, angle, length_dx, camera_intrinsics, verbose=False):
        if verbose:
            print('x: ', x)
            print('y: ', y)
            print('z: ', z)
            print('angle: ', angle)
            print('length_dx: ', length_dx)


        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length_dx / 2)
        p2[0][0] = int(length_dx / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)

        p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
        p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))


        p_grasp_right = self.project_image_point_to_pointcloud(p2_recentered[0], p2_recentered[1], z, camera_intrinsics)

        p_grasp_left = self.project_image_point_to_pointcloud(p1_recentered[0], p1_recentered[1], z, camera_intrinsics)

        length_in_meters = np.linalg.norm(np.asarray(p_grasp_right) - np.asarray(p_grasp_left)) + self.fix_value_meter_to_add

        if verbose:
            # print('center: ', p_grasp)
            print('right_finger: ', p_grasp_right)
            print('left_finger: ', p_grasp_left)
            print('length_in_meters: ', length_in_meters)

        return length_in_meters

    def visualise_gradients(self, depth_img):
        grad_x = cv2.Sobel(depth_img, cv2.CV_64F, 1, 0, borderType=cv2.BORDER_DEFAULT)
        grad_y = cv2.Sobel(depth_img, cv2.CV_64F, 0, 1, borderType=cv2.BORDER_DEFAULT)
        grad = np.sqrt(grad_x ** 2 + grad_y ** 2)
        rgb_gradients = np.dstack((np.asarray(grad_x), np.asarray(grad_y), np.asarray(grad))).astype(
            np.float32)

        # img is rgb, convert to opencv's default bgr
        cv2_img = cv2.cvtColor(rgb_gradients, cv2.COLOR_RGB2BGR)

        img = Image.fromarray(cv2_img)
        img.show()

    def visualize(self, grasps, rgb, segmask=None):
        """Visualize point cloud and last batch of computed grasps in a 2D visualizer
        """
        print('visualize')
        cv2_img = generate_image_with_grasps(grasps, rgb.copy(), destination=None, b_with_rectangles = False, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False)

        if segmask is not None:
            YELLOW = (0, 255, 255)
            centres = find_center_of_mask(segmask)
            # print('centres: ', centres)
            for c in centres:
                print('c: ', c)
                cv2_img = cv2.circle(cv2_img, (c[1], c[0]), 3, YELLOW, thickness=2, lineType=4)

        window_name = 'cv grasps'
        # window = cv2.namedWindow(window_name)
        #
        # # Using cv2.imshow() method
        # # Displaying the image
        # cv2.imshow(window_name, cv2_img)
        #
        # # waits for user to press any key
        # # (this is necessary to avoid Python kernel form crashing)
        # pressed_key = cv2.waitKey(0)
        # # closing all open windows
        # cv2.destroyAllWindows()
        cv2_img = cv2.cvtColor(cv2_img, cv2.COLOR_RGB2BGR)
        img = Image.fromarray(cv2_img)
        img.show(window_name)
        input('enter any key to continue:')

def generate_image_with_grasps(grasps, cv2_img, destination=None, b_with_rectangles = True, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False):
    scores = [g[11] for g in grasps]
    if normalize_scores:
        # norm = np.linalg.norm(scores)
        # scores = scores / norm
        current_maxval = np.max(scores)
        current_minval = np.min(scores)
    else:
        current_minval = 0.0
        current_maxval = 1.0
    # print("minval: ", current_minval)
    # print("minval: ", current_maxval)

    # for grasp in grasps:
    for i in range(len(grasps)):
        grasp = grasps[i]
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        x = grasp[3]
        y = grasp[4]
        angle = grasp[8]
        if default_color is None:
            # color = convert_to_rgb(grasp[11])
            color = convert_to_rgb(scores[i], minval=current_minval, maxval=current_maxval)

        else:
            color = default_color
        length = grasp[9]
        width = grasp[10]


        if b_with_rectangles:
            rectangle = grasp_to_rectangle(center=(y, x),
                                           angle=angle, length=length,
                                           width=width)

            rectangle = rectangle.astype('int32')

            blue = (255, 0, 0)
            yellow = (51, 255, 255)
            purple = (204, 204, 0)
            orange = (0, 128, 255)

            if b_with_circles:
                cv2_img = cv2.circle(cv2_img, (rectangle[0, 1], rectangle[0, 0]), 3, blue, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[1, 1], rectangle[1, 0]), 3, yellow, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[2, 1], rectangle[2, 0]), 3, orange, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[3, 1], rectangle[3, 0]), 3, purple, thickness=1, lineType=4)
            grasp_thickness = 1
            cv2_img = cv2.line(cv2_img, (rectangle[0, 1], rectangle[0, 0]), (rectangle[1, 1], rectangle[1, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[1, 1], rectangle[1, 0]), (rectangle[2, 1], rectangle[2, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[2, 1], rectangle[2, 0]), (rectangle[3, 1], rectangle[3, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[3, 1], rectangle[3, 0]), (rectangle[0, 1], rectangle[0, 0]),
                               color,
                               grasp_thickness, lineType=4)

        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length / 2)
        p2[0][0] = int(length / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)
        try:
            p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
            p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
        except:
            p1_recentered = (int(str(p1_rotated[0][0])) + int(str(x)), int(str(p1_rotated[1][0])) + int(str(y)))
            p2_recentered = (int(str(p2_rotated[0][0])) + int(str(x)), int(str(p2_rotated[1][0])) + int(str(y)))


        cv2_img = cv2.line(cv2_img, p1_recentered, p2_recentered,
                           color, thickness=thickness_line)#todo hardcoded

    if destination is not None:
        print("saving image to ", destination)
        cv2.imwrite(destination, cv2_img)

    return cv2_img

def convert_to_rgb(val, minval=0.0, maxval=1.0, colors=[(0, 0, 255), (255, 0, 0), (0, 255, 0)]):
    if val > maxval:
        val = maxval
    EPSILON = sys.float_info.epsilon  # Smallest possible difference.
    # "colors" is a series of RGB colors delineating a series of
    # adjacent linear color gradients between each pair.
    # Determine where the given value falls proportionality within
    # the range from minval->maxval and scale that fractional value
    # by the total number in the "colors" pallette.
    try:
        i_f = float(val - minval) / float(maxval - minval) * (len(colors) - 1)
    except Exception as e:
        print(e)
        i_f = 1.0
    # Determine the lower index of the pair of color indices this
    # value corresponds and its fractional distance between the lower
    # and the upper colors.
    try:
        i, f = int(i_f // 1), i_f % 1  # Split into whole & fractional parts.
    except Exception as e:
        print(e)
        f = 0.0
        i=0
    # Does it fall exactly on one of the color points?
    if f < EPSILON:
        return colors[i]
    else:  # Otherwise return a color within the range between them.
        (r1, g1, b1), (r2, g2, b2) = colors[i], colors[i + 1]
        return (int(r1 + f * (r2 - r1)), int(g1 + f * (g2 - g1)), int(b1 + f * (b2 - b1)))


def grasp_to_rectangle(center, angle, length, width, verbose = False):
    if verbose:
        print("from my_grasp_to_rectangle")
        print("center: ", center)
        print("angle: ", angle)
        print("length: ", length)
        print("width: ", width)

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))
    p3 = np.zeros((2, 1))
    p4 = np.zeros((2, 1))

    p1[0][0] = -int(length / 2)
    p1[1][0] = -int(width / 2)

    p2[0][0] = int(length / 2)
    p2[1][0] = -int(width / 2)

    p3[0][0] = int(length / 2)
    p3[1][0] = int(width / 2)

    p4[0][0] = -int(length / 2)
    p4[1][0] = int(width / 2)

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)
    p3_rotated = np.dot(rotation_matrix, p3)
    p4_rotated = np.dot(rotation_matrix, p4)

    p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
    p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
    p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
    p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

    # rectangle = np.array([p1_recentered,p2_recentered,p3_recentered,p4_recentered]).reshape((4,2)).astype(int)
    rectangle = np.array([p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
        (4, 2)).astype(int)
    return rectangle
