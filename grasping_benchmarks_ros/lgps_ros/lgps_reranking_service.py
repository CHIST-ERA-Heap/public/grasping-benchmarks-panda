#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import rospy
import sys
import os

from geometry_msgs.msg import PoseStamped

from grasping_benchmarks.base.transformations import quaternion_to_matrix
from grasping_benchmarks_ros.srv import Reranking, RerankingRequest, RerankingResponse
from grasping_benchmarks_ros.srv import GraspPlannerRequest
from grasping_benchmarks.lgps.lgps import LGPS


# Import the LGPS implementation code
# Requires a LGPS_DIR environment variable pointing to the root of the repo
sys.path.append(os.environ['LGPS_DIR'])
os.chdir(os.environ['LGPS_DIR'])
from scripts.grasps_generators.grasps_generators import transform_world_grasp_to_cam, transform_6Dgrasp_to_pixels, get_grasps_as_vectors

DEBUG = True

GraspPlannerResponse = RerankingResponse


class LGPSService(LGPS):
    def __init__(self, cfg_file : str, cv_bridge : CvBridge, reranking_service_name : str, reranking_publisher_name : str):
        super(LGPSService, self).__init__(cfg_file)
        self.cv_bridge = cv_bridge

        # Create publisher to publish the reranked grasp.
        self.grasp_pose_publisher = None
        if reranking_publisher_name is not None:
            self.grasp_pose_publisher = rospy.Publisher(reranking_publisher_name, PoseStamped, queue_size=10)

        # Initialize the ROS segmentation service.
        self._reranking_service = rospy.Service(reranking_service_name, Reranking,
                                            self.reranking_handler)

    def read_images(self, req : RerankingRequest):
        """Read images as a CameraData class from a service request

        Parameters
        ----------
        req : GraspPlannerRequest
            ROS service request for the grasp planning service
        """
        camera_info = req.camera_info
        viewpoint = req.view_point
        try:
            cv2_color = self.cv_bridge.imgmsg_to_cv2(req.color_image, desired_encoding='bgr8')
            cv2_color = np.array(cv2_color, dtype=np.uint8)

            raw_depth = self.cv_bridge.imgmsg_to_cv2(req.depth_image, desired_encoding='passthrough')
            cv2_depth = np.array(raw_depth, dtype=np.float32)

            cv2_depth *= 0.001

            # Fix NANs
            nans_idx = np.isnan(cv2_depth)
            cv2_depth[nans_idx] = 1000.0

        except CvBridgeError as cv_bridge_exception:
            rospy.logerr(cv_bridge_exception)

        # Check for image and depth size
        if (cv2_color.shape[0] != cv2_depth.shape[0]) or (cv2_color.shape[1] != cv2_depth.shape[1]):
            msg = "Mismatch between depth shape {}x{} and color shape {}x{}".format(cv2_depth.shape[0],
                                                                                    cv2_depth.shape[1],
                                                                                    cv2_color.shape[0],
                                                                                    cv2_color.shape[1])
            rospy.logerr(msg)
            raise rospy.ServiceException(msg)

        # print('grconvnet_grasp_planner_service: read_images 1')
        #import ipdb; ipdb.set_trace()
        if req.seg_image.height > 0:
            segmentation_mask = self.cv_bridge.imgmsg_to_cv2(req.seg_image, desired_encoding='passthrough')
        else:
            segmentation_mask = None

        camera_position = np.array([viewpoint.pose.position.x,
                                    viewpoint.pose.position.y,
                                    viewpoint.pose.position.z])
        camera_orientation = quaternion_to_matrix([viewpoint.pose.orientation.x,
                                           viewpoint.pose.orientation.y,
                                           viewpoint.pose.orientation.z,
                                           viewpoint.pose.orientation.w])
        camera_extrinsic_matrix = np.eye(4)
        camera_extrinsic_matrix[:3,:3] = camera_orientation
        camera_extrinsic_matrix[:3,3] = camera_position

        obj_cloud = None

        camera_data = self.create_camera_data(rgb_image=cv2_color,
                                              depth_image=cv2_depth,
                                              seg_mask = segmentation_mask,
                                              cam_intrinsic_frame=camera_info.header.frame_id,
                                              cam_extrinsic_matrix=camera_extrinsic_matrix,
                                              fx=camera_info.K[0],
                                              fy=camera_info.K[4],
                                              cx=camera_info.K[2],
                                              cy=camera_info.K[5],
                                              skew=0.0,
                                              w=camera_info.width,
                                              h=camera_info.height,
                                              obj_cloud=obj_cloud)
        return camera_data

    def get_grasps_as_vectors(self, grasps):
        return get_grasps_as_vectors(grasps, self._camera_data.extrinsic_matrix, self._camera_data.intrinsic_params, scene_id=-1, object_id=-1,
                              grasp_planner_database_id=-1, fix_pixel_width=38)

    def reranking_handler(self, req : RerankingRequest) -> GraspPlannerResponse:
        print('lgps_service: reranking_handler')

        # Read camera images from the request
        self._camera_data = self.read_images(req)

        grasps = req.grasp_candidates
        grasp_list_pixels = self.get_grasps_as_vectors(grasps)

        # Set number of candidates
        n_of_candidates = req.n_of_candidates if req.n_of_candidates else 1

        ok = self.rerank(grasp_list_pixels, n_candidates=n_of_candidates)
        # print('grconvnet_grasp_planner_service: plan_grasp passed')

        if ok:
            self.camera_viewpoint = req.view_point
            return self._create_grasp_planner_srv_msg(grasps, grasp_list_pixels)
        else:
            #todo return previous
            return GraspPlannerResponse()

    def _create_grasp_planner_srv_msg(self, grasps, grasp_list_pixels) -> GraspPlannerResponse:
        """Create service response message

        Returns
        -------
        GraspPlannerResponse
            Service response message
        """

        response = GraspPlannerResponse()

        if self.predict_gripper_opening and self.length_regressor is not None:
            length_meters_list = self.get_list_of_lengths_in_meters(grasp_list_pixels)
        for i in range(len(grasp_list_pixels)):
            grasp_msg = grasps[i]
            # Set candidate score and width
            grasp_msg.score.data = grasp_list_pixels[i][11]
            if self.predict_gripper_opening and self.length_regressor is not None:
                grasp_msg.width.data = length_meters_list[i]

            response.grasp_candidates.append(grasp_msg)

        #todo
        if self.grasp_pose_publisher is not None:
            # Publish poses for direct rViz visualization
            # TODO: properly publish all the poses
            print("Publishing grasps on topic")
            self.grasp_pose_publisher.publish(response.grasp_candidates[0].pose)

        return response


if __name__ == "__main__":

    # Initialize the ROS node
    rospy.init_node("lgps_node")

    # Initialize CvBridge
    cv_bridge = CvBridge()

    # Get configuration options
    cfg_file = rospy.get_param("~config_file_lgps")
    lgps_service_name = rospy.get_param("~lgps_service_name")
    lgps_publisher_name = rospy.get_param("~lgps_publisher_name")

    # Initialize the grasp planner service
    grasp_planner = LGPSService(cfg_file=cfg_file,
                                                cv_bridge=cv_bridge,
                                                reranking_service_name=lgps_service_name,
                                                reranking_publisher_name=lgps_publisher_name)

    rospy.loginfo("Reranking Policy Initiated")

    # Spin forever.
    rospy.spin()






