#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import rospy
import ros_numpy
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Header
from sensor_msgs.msg import PointCloud2, PointField
from sensor_msgs import point_cloud2 as pc2
from grasping_benchmarks.base.transformations import quaternion_to_matrix, matrix_to_quaternion
from grasping_benchmarks.base.base_grasp_planner import CameraData
from grasping_benchmarks_ros.srv import Segmentation, SegmentationRequest, SegmentationResponse
from grasping_benchmarks_ros.msg import BenchmarkGrasp
from grasping_benchmarks.segmentation.segmentation_code import SegmentationClass
import configparser
from sensor_msgs.msg import Image
DEBUG = True

class SegmentationService(SegmentationClass):
    #segmentation_service_name='rgb/image_seg'
    def __init__(self, cfg_file : str, cv_bridge : CvBridge, segmentation_service_name : str, segmentation_publisher_name : str):

        # config = configparser.ConfigParser()
        # config.read(cfg_file)

        super(SegmentationService, self).__init__(cfg_file)
        self.cv_bridge = cv_bridge

        # Create publisher to publish the segmentation mask.
        self.segmentation_publisher = None
        if segmentation_service_name is not None:
            self.segmentation_publisher = rospy.Publisher(segmentation_publisher_name, Image, queue_size=10)

        # Initialize the ROS segmentation service.
        self._segmentation_service = rospy.Service(segmentation_service_name, Segmentation,
                                            self.segment_handler)

    def transform_pc_to_camera_frame(self, pc : np.ndarray, camera_pose : np.ndarray) -> np.ndarray:
        """Transform the point cloud from root to camera reference frame

        Parameters
        ----------
        pc : np.ndarray
            nx3, float64 array of points
        camera_pose : np.ndarray
            4x4 camera pose, affine transformation

        Returns
        -------
        np.ndarray
            [description]
        """

        # [F]_p         : point p in reference frame F
        # [F]_T_[f]     : frame f in reference frame F
        # r             : root ref frame
        # cam           : cam ref frame
        # p, pc         : point, point cloud (points as rows)
        # tr(r_pc) = r_T_cam * tr(cam_pc)
        # tr(cam_pc) = inv(r_T_cam) * tr(r_pc)

        r_pc = np.c_[pc, np.ones(pc.shape[0])]
        cam_T_r = np.linalg.inv(camera_pose)
        cam_pc = np.transpose(np.dot(cam_T_r, np.transpose(r_pc)))

        return cam_pc[:, :-1]

    def read_images(self, req : SegmentationRequest):
        """Read images as a CameraData class from a service request

        Parameters
        ----------
        req : SegmentationRequest
            ROS service request for the grasp planning service
        """

        # Get color, depth and camera parameters from request
        camera_info = req.camera_info
        # viewpoint = req.view_point
        try:
            #print('with bgr8')
            #cv2_color = self.cv_bridge.imgmsg_to_cv2(req.color_image, desired_encoding='rgb8')
            cv2_color = self.cv_bridge.imgmsg_to_cv2(req.color_image, desired_encoding='bgr8')
            cv2_color = np.array(cv2_color, dtype=np.uint8)

            raw_depth = self.cv_bridge.imgmsg_to_cv2(req.depth_image, desired_encoding='passthrough')
            cv2_depth = np.array(raw_depth, dtype=np.float32)

            cv2_depth *= 0.001

            # Fix NANs
            nans_idx = np.isnan(cv2_depth)
            cv2_depth[nans_idx] = 1000.0

        except CvBridgeError as cv_bridge_exception:
            rospy.logerr(cv_bridge_exception)

        # print('try segmentation_service cv2_color_background')
        try:
            cv2_color_background = self.cv_bridge.imgmsg_to_cv2(req.background_image, desired_encoding='rgb8')
        except CvBridgeError as cv_bridge_exception:
            #rospy.logwarn(cv_bridge_exception)
            print('warning because no background image provided')
            cv2_color_background = None
        # print('passed segmentation_service cv2_color_background')

        # Check for image and depth size
        if (cv2_color.shape[0] != cv2_depth.shape[0]) or (cv2_color.shape[1] != cv2_depth.shape[1]):
            msg = "Mismatch between depth shape {}x{} and color shape {}x{}".format(cv2_depth.shape[0],
                                                                                    cv2_depth.shape[1],
                                                                                    cv2_color.shape[0],
                                                                                    cv2_color.shape[1])
            rospy.logerr(msg)
            raise rospy.ServiceException(msg)

        camera_data = self.create_camera_data(rgb_image=cv2_color,
                                              depth_image=cv2_depth,
                                              background_image=cv2_color_background,
                                              cam_intrinsic_frame=camera_info.header.frame_id,
                                              cam_extrinsic_matrix=None,
                                              fx=camera_info.K[0],
                                              fy=camera_info.K[4],
                                              cx=camera_info.K[2],
                                              cy=camera_info.K[5],
                                              skew=0.0,
                                              w=camera_info.width,
                                              h=camera_info.height,
                                              obj_cloud=None)#todo
        print('segmentation_service camera_data filled')

        return camera_data

    def segment_handler(self, req : SegmentationRequest) -> SegmentationResponse:

        # Read camera images from the request
        camera_data = self.read_images(req)

        ok = self.segment(camera_data)
        if ok:
            return self._create_seg_srv_msg()
        else:
            return SegmentationResponse()

    def _create_seg_srv_msg(self) -> SegmentationResponse:

        response = SegmentationResponse()


        # Segmentation image
        header = Header()
        seg = self.computed_mask.astype(np.uint8)
        seg_img = ros_numpy.msgify(Image, seg, encoding='mono8')
        seg_img.header = header
        response.seg_image = seg_img

        print('segmentation_service sending response')
        return response


if __name__ == "__main__":

    # Initialize the ROS node
    rospy.init_node("segmentation_node")

    # Initialize CvBridge
    cv_bridge = CvBridge()

    # Get configuration options
    cfg_file = rospy.get_param("~config_file_seg")
    segmentation_service_name = rospy.get_param("~segmentation_service_name")
    segmentation_publisher_name = rospy.get_param("~segmentation_publisher_name")

    # Initialize the grasp planner service
    grasp_planner = SegmentationService(cfg_file=cfg_file,
                                                cv_bridge=cv_bridge,
                                                segmentation_service_name=segmentation_service_name,
                                                segmentation_publisher_name=segmentation_publisher_name)

    rospy.loginfo("Segmentation Policy Initiated")

    # Spin forever.
    rospy.spin()






