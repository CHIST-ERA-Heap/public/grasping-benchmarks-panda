<launch>

  <arg name="gpd"           default="false" />
  <arg name="superquadrics" default="false" />
  <arg name="graspnet"      default="false" />
  <arg name="dexnet"        default="false"  />
  <arg name="grconvnet"     default="false"  />
  <arg name="cvgrasp"     default="false"  />

  <!-- Run Dexnet Grasp Planner -->
  <group if="$(arg dexnet)">

    <arg name="ns"  default="dexnet_bench" />
    <arg name="grasp_planner_service_name"        default="dexnet_grasp_planner_service" />
    <arg name="grasp_planner_service"             default="GraspPlanner" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />

    <arg name="model_dir"                         default="/workspace/sources/gqcnn/models" />
    <arg name="fully_conv"                        default="false" />
    <arg name="model_name"                        default="FC-GQCNN-4.0-PJ" if="$(eval fully_conv == true)" />
    <arg name="model_name"                        default="GQCNN-4.0-PJ" if="$(eval fully_conv == false)" />

    <!-- <arg name="grasp_pose_offset"                 default="[0.0, 0.0, -0.02]" /> -->
    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.005]" />


    <node name="dexnet_grasp_planner"  pkg="grasping_benchmarks_ros" type="dexnet_grasp_planner_service.py" ns="$(arg ns)" output="screen" >
    	<param name="model_name"                          value="$(arg model_name)" />
    	<param name="model_dir"                           value="$(arg model_dir)" />
    	<param name="fully_conv"                          value="$(arg fully_conv)" />
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <rosparam param="grasp_pose_offset"               subst_value="True">$(arg grasp_pose_offset)</rosparam>
    </node>

  </group>

  <!-- Run Superquadrics-based Grasp Planner -->
  <group if="$(arg superquadrics)">

    <arg name="ns"                        default="superquadrics_bench" />
    <arg name="grasp_planner_node_name"   default="superq_grasp_planner" />

    <arg name="grasp_planner_service_name"        default="superq_grasp_planner_service" />
    <arg name="grasp_planner_service"             default="GraspPlannerCloud" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />
    <arg name="config_file"                       default="$(find grasping_benchmarks_ros)/superquadrics_based_ros/cfg/config_panda.yaml" />

    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.0]" />

    <node name="$(arg grasp_planner_node_name)"  pkg="grasping_benchmarks_ros"  type="superq_grasp_planner_service.py" ns="$(arg ns)" output="screen" >
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <param name="config_file"                         value="$(arg config_file)" />
      <rosparam param="grasp_pose_offset"             subst_value="True">$(arg grasp_pose_offset)</rosparam>
    </node>


  </group>

  <!-- Run GraspNet Grasp Planner -->
  <group if="$(arg graspnet)" >

    <!-- Planner params -->
    <arg name="ns"                          default="graspnet_bench" />
    <arg name="grasp_planner_node_name"     default="graspnet_grasp_planner" />
    <arg name="grasp_planner_service_name"        default="graspnet_grasp_planner_service" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />
    <arg name="config_file"                       default="$(find grasping_benchmarks_ros)/graspnet_ros/cfg/cfg_graspnet.yaml" />
    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.11]" />
    <arg name="grasp_planner_service"             default="GraspPlannerCloud" />
    <!-- this last one is unused, left for legacy will have to be removed -->

    <!-- Launch planner -->
    <node name="graspnet_grasp_planner" pkg="grasping_benchmarks_ros" type="graspnet_grasp_planner_service.py" ns="$(arg ns)" output="screen">
      <param name="grasp_planner_node_name"             value="$(arg grasp_planner_node_name)" />
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <param name="config_file"                         value="$(arg config_file)" />
      <rosparam param="grasp_pose_offset"             subst_value="True">$(arg grasp_pose_offset)</rosparam>
    </node>

  </group>

    <!-- Run GPD Grasp Planner -->
  <group if="$(arg gpd)">

    <arg name="ns"  default="gpd_bench" />
    <arg name="grasp_planner_node_name" default="gpd_grasp_planner" />

    <arg name="grasp_planner_service_name"        default="gpd_grasp_planner_service" />
    <arg name="grasp_planner_service"             default="GraspPlannerCloud" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />
    <arg name="config_file"                       default="$(find grasping_benchmarks_ros)/gpd_ros/cfg/ros_caffe_params.cfg" />
    <arg name="publish_rviz"                      default="true" />
    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.05]" />

    <node name="$(arg grasp_planner_node_name)"  pkg="grasping_benchmarks_ros"  type="gpd_grasp_planner_service" ns="$(arg ns)" output="screen" >
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <param name="config_file"                         value="$(arg config_file)" />
      <param name="publish_rviz"                        value="$(arg publish_rviz)" />
      <rosparam param="grasp_pose_offset"             subst_value="True">$(arg grasp_pose_offset)</rosparam>
    </node>


  </group>


    <!-- Run GRConvNet Grasp Planner -->
  <group if="$(arg grconvnet)" >

    <!-- Planner params -->
    <arg name="ns"                          default="grconvnet_bench" />
    <arg name="grasp_planner_node_name"     default="grconvnet_grasp_planner" />
    <arg name="grasp_planner_service_name"        default="grconvnet_grasp_planner_service" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />
    <arg name="config_file"                       default="$(find grasping_benchmarks_ros)/grconvnet_ros/cfg/cfg_grconvnet.ini" />
    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.03]" />
    <arg name="grasp_planner_service"             default="GraspPlannerCloud" />
    <!-- this last one is unused, left for legacy will have to be removed -->
    <arg name="to_world"     default="true"  />

    <!-- Launch planner -->
    <node name="grconvnet_grasp_planner" pkg="grasping_benchmarks_ros" type="grconvnet_grasp_planner_service.py" ns="$(arg ns)" output="screen">
      <param name="grasp_planner_node_name"             value="$(arg grasp_planner_node_name)" />
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <param name="config_file"                         value="$(arg config_file)" />
      <rosparam param="grasp_pose_offset"               subst_value="True">$(arg grasp_pose_offset)</rosparam>
      <param name="to_world"                            value="$(arg to_world)" />

    </node>

  </group>
  
  <!-- Run CV Grasp Planner -->
  <group if="$(arg cvgrasp)" >

    <!-- Planner params -->
    <arg name="ns"                                default="cvgrasp_bench" />
    <arg name="grasp_planner_node_name"           default="cvgrasp_planner" />
    <arg name="grasp_planner_service_name"        default="cvgrasp_planner_service" />
    <arg name="grasp_publisher_name"              default="candidate_grasp" />
    <arg name="config_file"                       default="$(find grasping_benchmarks_ros)/cvgrasp_ros/cfg/cfg_cvgrasp.ini" />
    <arg name="grasp_pose_offset"                 default="[0.0, 0.0, 0.03]" />
    <arg name="grasp_planner_service"             default="GraspPlannerCloud" />
    <!-- this last one is unused, left for legacy will have to be removed -->
    <arg name="to_world"     default="true"  />

    <!-- Launch planner -->
    <node name="cvgrasp_planner" pkg="grasping_benchmarks_ros" type="cvgrasp_planner_service.py" ns="$(arg ns)" output="screen">
      <param name="grasp_planner_node_name"             value="$(arg grasp_planner_node_name)" />
      <param name="grasp_planner_service_name"          value="$(arg grasp_planner_service_name)" />
      <param name="grasp_publisher_name"                value="$(arg grasp_publisher_name)" />
      <param name="config_file"                         value="$(arg config_file)" />
      <rosparam param="grasp_pose_offset"               subst_value="True">$(arg grasp_pose_offset)</rosparam>
      <param name="to_world"                            value="$(arg to_world)" />

    </node>

  </group>

</launch>
